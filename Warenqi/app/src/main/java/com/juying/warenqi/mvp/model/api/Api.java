package com.juying.warenqi.mvp.model.api;

public interface Api {
    String APP_DOMAIN = "http://121.201.29.42:9005/";
    String QINIU_URL = "http://7qndaf.com2.z0.glb.qiniucdn.com/";
    String NOTICE_JOINT_URL = APP_DOMAIN + "wap/helpAndNoticeDetail/";//公告待拼接链接
    String NOTICE_LIST = APP_DOMAIN + "wap/noticeCenter";//公告列表
    String BEGINNER_GUIDE = APP_DOMAIN + "wap/helpCenter";//新手指导
    String INVITE_BUYER = APP_DOMAIN + "wap/inviteReg?id=";//邀请买手
    String INVITE_SELLER = APP_DOMAIN + "/regist/SELLER?id=";//邀请商家
    int SUCCESS_CODE = 200;
}
