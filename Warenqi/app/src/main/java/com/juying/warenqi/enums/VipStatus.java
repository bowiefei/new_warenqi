package com.juying.warenqi.enums;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/28 18:19
 * </pre>
 */
public enum VipStatus {
    LOW("降权用户"),
    NORMAL("普通会员"),
    VIP1("VIP1"),
    VIP2("VIP2"),
    VIP3("VIP3"),
    SPECIAL("合作用户");

    private final String type;

    VipStatus(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}

