package com.juying.warenqi.mvp.ui.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blankj.utilcode.util.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.model.entity.AccountInfoSectionContent;
import com.juying.warenqi.mvp.ui.holder.BaseAutoLayoutHolder;

import java.util.List;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/4 11:43
 * </pre>
 */
public class CommonAdapter<T> extends BaseQuickAdapter<T, BaseAutoLayoutHolder> {
    public CommonAdapter(@LayoutRes int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseAutoLayoutHolder helper, T item) {
        if (item instanceof AccountInfoSectionContent) {
            String extras = ((AccountInfoSectionContent) item).getExtras();
            helper.setText(R.id.tv_account_item_title, ((AccountInfoSectionContent) item).getTitle())
                    .setText(R.id.tv_account_item_extra,
                            TextUtils.isEmpty(extras) ? "" : extras)
                    .setTextColor(R.id.tv_account_item_extra, mContext.getApplicationContext()
                            .getResources().getColor(R.color.grey_999));
            View view = helper.getView(R.id.item_container);
            view.setPadding(SizeUtils.dp2px(15), 0, 0, 0);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.height = SizeUtils.dp2px(50);
            view.setLayoutParams(layoutParams);
            ((TextView) helper.getView(R.id.tv_account_item_title)).setTextSize(16);
            TextView textView = helper.getView(R.id.tv_account_item_extra);
            textView.setTextSize(14);
            if (!TextUtils.isEmpty(extras))
                if (((AccountInfoSectionContent) item).getLabelResId() == 0)
                    switch (extras) {
                        case "编辑中":
                        case "待审核":
                        case "审核中":
                        case "审核未通过":
                            textView.setTextColor(0xFFFF5722);
                            break;
                        case "审核通过":
                            textView.setTextColor(0xFF32BBA0);
                            break;
                        case "封号中":
                            textView.setTextColor(0xFF999999);
                            break;
                    }
                else if (((AccountInfoSectionContent) item).getLabelResId() == 1)
                    switch (extras) {
                        case "待审核":
                            textView.setText("审核中");
                            textView.setTextColor(mContext.getResources().getColor(R.color.yellow_ffb64d));
                            break;
                        case "审核通过":
                            textView.setText("已认证");
                            textView.setTextColor(mContext.getResources().getColor(R.color.grey_333));
                            break;
                        case "审核未通过":
                            textView.setText("审核不通过");
                            textView.setTextColor(mContext.getResources().getColor(R.color.orange_ff5757));
                            break;
                        default:
                            textView.setText("未认证");
                            textView.setTextColor(mContext.getResources().getColor(R.color.grey_999fb6));
                            break;
                    }
        }
    }
}
