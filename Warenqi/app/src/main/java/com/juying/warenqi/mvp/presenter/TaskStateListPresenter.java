package com.juying.warenqi.mvp.presenter;

import android.app.Application;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.widget.imageloader.ImageLoader;
import com.juying.warenqi.mvp.contract.TaskStateListContract;
import com.juying.warenqi.mvp.model.entity.AskTaskState;
import com.juying.warenqi.mvp.model.entity.FlowTaskState;
import com.juying.warenqi.mvp.model.entity.PayTaskState;
import com.juying.warenqi.mvp.ui.adapter.TaskStateAdapter;
import com.juying.warenqi.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class TaskStateListPresenter extends BasePresenter<TaskStateListContract.Model, TaskStateListContract.View> {
    private RxErrorHandler mErrorHandler;
    private Application mApplication;
    private ImageLoader mImageLoader;
    private AppManager mAppManager;
    private BaseQuickAdapter mAdapter;
    private boolean isFirst = true;
    private long idLastQueried = 1;
    private List<FlowTaskState> mFlowTaskStates = new ArrayList<>();
    private List<PayTaskState> mPayTaskStates = new ArrayList<>();
    private List<AskTaskState> mAskTaskStates = new ArrayList<>();

    @Inject
    public TaskStateListPresenter(TaskStateListContract.Model model, TaskStateListContract.View rootView
            , RxErrorHandler handler, Application application
            , ImageLoader imageLoader, AppManager appManager) {
        super(model, rootView);
        this.mErrorHandler = handler;
        this.mApplication = application;
        this.mImageLoader = imageLoader;
        this.mAppManager = appManager;
    }

    public void getFlowTaskList(String state, int pageNo, Long id, boolean isUpdate) {
        if (mAdapter == null) {
            mAdapter = new TaskStateAdapter<>(mFlowTaskStates);
            mRootView.setAdapter(mAdapter);
        }

        if (isUpdate) idLastQueried = 1;

        boolean isEvictCache = isUpdate;//是否驱逐缓存,为true即不使用缓存,每次下拉刷新即需要最新数据,则不使用缓存

        if (isUpdate && isFirst) {//默认在第一次上拉刷新时使用缓存
            isFirst = false;
            isEvictCache = false;
        }

        mModel.getFlowTaskStateList(state, pageNo, id, idLastQueried, isEvictCache)
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<List<FlowTaskState>>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull List<FlowTaskState> flowTaskStates) {
                        if (flowTaskStates.size() > 0)
                            idLastQueried = flowTaskStates.get(flowTaskStates.size() - 1).getId();//记录最后一个id,用于下一次请求
                        mAdapter.removeAllFooterView();
                        if (isUpdate) {
                            mAdapter.setNewData(flowTaskStates);
                        } else {
                            mAdapter.addData(flowTaskStates);
                            if (flowTaskStates.size() == 0) {
                                mRootView.setNoMoreData(mAdapter);
                            }
                        }
                    }
                });
    }

    public void getPayTaskList(String state, int pageNo, Long id, boolean isUpdate) {
        if (mAdapter == null) {
            mAdapter = new TaskStateAdapter<>(mPayTaskStates);
            mRootView.setAdapter(mAdapter);
        }

        if (isUpdate) idLastQueried = 1;

        boolean isEvictCache = isUpdate;//是否驱逐缓存,为true即不使用缓存,每次下拉刷新即需要最新数据,则不使用缓存

        if (isUpdate && isFirst) {//默认在第一次上拉刷新时使用缓存
            isFirst = false;
            isEvictCache = false;
        }

        mModel.getPayTaskStateList(state, pageNo, id, idLastQueried, isEvictCache)
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<List<PayTaskState>>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull List<PayTaskState> payTaskStates) {
                        if (payTaskStates.size() > 0)
                            idLastQueried = payTaskStates.get(payTaskStates.size() - 1).getId();//记录最后一个id,用于下一次请求
                        mAdapter.removeAllFooterView();
                        if (isUpdate) {
                            mAdapter.setNewData(payTaskStates);
                        } else {
                            mAdapter.addData(payTaskStates);
                            if (payTaskStates.size() == 0) {
                                mRootView.setNoMoreData(mAdapter);
                            }
                        }
                    }
                });
    }

    public void getAskTaskList(String state, int pageNo, Long id, boolean isUpdate) {
        if (mAdapter == null) {
            mAdapter = new TaskStateAdapter<>(mAskTaskStates);
            mRootView.setAdapter(mAdapter);
        }

        if (isUpdate) idLastQueried = 1;

        boolean isEvictCache = isUpdate;//是否驱逐缓存,为true即不使用缓存,每次下拉刷新即需要最新数据,则不使用缓存

        if (isUpdate && isFirst) {//默认在第一次上拉刷新时使用缓存
            isFirst = false;
            isEvictCache = false;
        }

        mModel.getAskTaskStateList(state, pageNo, id, idLastQueried, isEvictCache)
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<List<AskTaskState>>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull List<AskTaskState> askTaskStates) {
                        if (askTaskStates.size() > 0)
                            idLastQueried = askTaskStates.get(askTaskStates.size() - 1).getId();//记录最后一个id,用于下一次请求
                        mAdapter.removeAllFooterView();
                        if (isUpdate) {
                            mAdapter.setNewData(askTaskStates);
                        } else {
                            mAdapter.addData(askTaskStates);
                            if (askTaskStates.size() == 0) {
                                mRootView.setNoMoreData(mAdapter);
                            }
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
        mAdapter = null;
        mAskTaskStates = null;
        mFlowTaskStates = null;
        mPayTaskStates = null;
    }

}