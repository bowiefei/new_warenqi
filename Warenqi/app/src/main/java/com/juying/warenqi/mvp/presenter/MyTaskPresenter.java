package com.juying.warenqi.mvp.presenter;

import android.app.Application;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.widget.imageloader.ImageLoader;
import com.juying.warenqi.R;
import com.juying.warenqi.enums.TaskStatus;
import com.juying.warenqi.mvp.contract.MyTaskContract;
import com.juying.warenqi.mvp.model.entity.MyTaskStateContent;
import com.juying.warenqi.mvp.model.entity.MyTaskStateSection;
import com.juying.warenqi.mvp.ui.adapter.MyTaskStateAdapter;
import com.juying.warenqi.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class MyTaskPresenter extends BasePresenter<MyTaskContract.Model, MyTaskContract.View> {
    private RxErrorHandler mErrorHandler;
    private Application mApplication;
    private ImageLoader mImageLoader;
    private AppManager mAppManager;
    private BaseQuickAdapter mAdapter;
    private List<MyTaskStateSection> mList = new ArrayList<>();

    @Inject
    public MyTaskPresenter(MyTaskContract.Model model, MyTaskContract.View rootView
            , RxErrorHandler handler, Application application
            , ImageLoader imageLoader, AppManager appManager) {
        super(model, rootView);
        this.mErrorHandler = handler;
        this.mApplication = application;
        this.mImageLoader = imageLoader;
        this.mAppManager = appManager;
        mList.clear();
    }

    public void getAdapter() {
        mList.clear();
        mList.add(new MyTaskStateSection(true,
                mApplication.getString(R.string.flow_task), R.color.blue_4680fe));

        MyTaskStateContent flowWaitOperateStatus =
                new MyTaskStateContent(mApplication.getString(R.string.flow_task),
                        TaskStatus.WAIT);
        MyTaskStateContent flowSellerOperateStatus =
                new MyTaskStateContent(mApplication.getString(R.string.flow_task),
                        TaskStatus.SELLER_PROCESS);
        MyTaskStateContent flowAppealStatus =
                new MyTaskStateContent(mApplication.getString(R.string.flow_task),
                        TaskStatus.APPEAL);
        MyTaskStateContent flowFinishedStatus =
                new MyTaskStateContent(mApplication.getString(R.string.flow_task),
                        TaskStatus.FINISHED);
        MyTaskStateContent flowCanceledStatus =
                new MyTaskStateContent(mApplication.getString(R.string.flow_task),
                        TaskStatus.CANCEL);
        mList.add(new MyTaskStateSection(flowWaitOperateStatus));
        mList.add(new MyTaskStateSection(flowSellerOperateStatus));
        mList.add(new MyTaskStateSection(flowAppealStatus));
        mList.add(new MyTaskStateSection(flowFinishedStatus));
        mList.add(new MyTaskStateSection(flowCanceledStatus));

        mList.add(new MyTaskStateSection(true,
                mApplication.getString(R.string.advance_pay_task), R.color.red_fb607f));

        MyTaskStateContent payWaitOperateStatus =
                new MyTaskStateContent(mApplication.getString(R.string.advance_pay_task),
                        TaskStatus.WAIT);
        MyTaskStateContent paySellerOperateStatus =
                new MyTaskStateContent(mApplication.getString(R.string.advance_pay_task),
                        TaskStatus.SELLER_PROCESS);
        MyTaskStateContent payWaitCommentStatus =
                new MyTaskStateContent(mApplication.getString(R.string.advance_pay_task),
                        TaskStatus.WAIT_COMMENT);
        MyTaskStateContent payAppealStatus =
                new MyTaskStateContent(mApplication.getString(R.string.advance_pay_task),
                        TaskStatus.APPEAL);
        MyTaskStateContent payFinishedStatus =
                new MyTaskStateContent(mApplication.getString(R.string.advance_pay_task),
                        TaskStatus.FINISHED);
        MyTaskStateContent payCanceledStatus =
                new MyTaskStateContent(mApplication.getString(R.string.advance_pay_task),
                        TaskStatus.CANCEL);
        mList.add(new MyTaskStateSection(payWaitOperateStatus));
        mList.add(new MyTaskStateSection(paySellerOperateStatus));
        mList.add(new MyTaskStateSection(payWaitCommentStatus));
        mList.add(new MyTaskStateSection(payAppealStatus));
        mList.add(new MyTaskStateSection(payFinishedStatus));
        mList.add(new MyTaskStateSection(payCanceledStatus));

        mList.add(new MyTaskStateSection(true,
                mApplication.getString(R.string.ask_task), R.color.yellow_ffb64d));

        MyTaskStateContent askWaitOperateStatus =
                new MyTaskStateContent(mApplication.getString(R.string.ask_task),
                        TaskStatus.WAIT);
        MyTaskStateContent askSellerOperateStatus =
                new MyTaskStateContent(mApplication.getString(R.string.ask_task),
                        TaskStatus.SELLER_PROCESS);
        MyTaskStateContent askAppealStatus =
                new MyTaskStateContent(mApplication.getString(R.string.ask_task),
                        TaskStatus.APPEAL);
        MyTaskStateContent askFinishedStatus =
                new MyTaskStateContent(mApplication.getString(R.string.ask_task),
                        TaskStatus.FINISHED);
        MyTaskStateContent askCanceledStatus =
                new MyTaskStateContent(mApplication.getString(R.string.ask_task),
                        TaskStatus.CANCEL);
        mList.add(new MyTaskStateSection(askWaitOperateStatus));
        mList.add(new MyTaskStateSection(askSellerOperateStatus));
        mList.add(new MyTaskStateSection(askAppealStatus));
        mList.add(new MyTaskStateSection(askFinishedStatus));
        mList.add(new MyTaskStateSection(askCanceledStatus));

        if (mAdapter == null) {
            mAdapter = new MyTaskStateAdapter(mList);
            mRootView.setAdapter(mAdapter);
        }
    }

    public void getFlowTaskCount() {
        mModel.getFlowTaskCount()
                .compose(RxUtils.applySchedulers(mRootView))
                .compose(RxUtils.bindToLifecycle(mRootView))
                .flatMap(flowTaskCount -> {
                    for (MyTaskStateSection taskStateSection : mList) {
                        if (!taskStateSection.isHeader) {
                            if (mApplication.getString(R.string.flow_task).equals(taskStateSection.t.getTaskType()))
                                switch (taskStateSection.t.getTaskStatus()) {
                                    case WAIT:
                                        taskStateSection.t.setTaskTaskCount(flowTaskCount.getWAIT());
                                        break;
                                    case SELLER_PROCESS:
                                        taskStateSection.t.setTaskTaskCount(flowTaskCount.getSELLER_PROCESS());
                                        break;
                                    case APPEAL:
                                        taskStateSection.t.setTaskTaskCount(flowTaskCount.getAPPEAL());
                                        break;
                                    case FINISHED:
                                        taskStateSection.t.setTaskTaskCount(flowTaskCount.getFINISHED());
                                        break;
                                    case CANCEL:
                                        taskStateSection.t.setTaskTaskCount(flowTaskCount.getCANCEL());
                                        break;
                                }
                        }
                    }
                    return Observable.just(mAdapter);
                })
                .subscribe(new ErrorHandleSubscriber<BaseQuickAdapter>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseQuickAdapter baseQuickAdapter) {
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
    }

    public void getAdvancedPayTaskCount() {
        mModel.getAdvancedPayTaskCount()
                .compose(RxUtils.applySchedulers(mRootView))
                .compose(RxUtils.bindToLifecycle(mRootView))
                .flatMap(payTaskCount -> {
                    for (MyTaskStateSection taskStateSection : mList) {
                        if (!taskStateSection.isHeader) {
                            if (mApplication.getString(R.string.advance_pay_task).equals(taskStateSection.t.getTaskType()))
                                switch (taskStateSection.t.getTaskStatus()) {
                                    case WAIT:
                                        taskStateSection.t.setTaskTaskCount(payTaskCount.getWAIT().getCount());
                                        break;
                                    case SELLER_PROCESS:
                                        taskStateSection.t.setTaskTaskCount(payTaskCount.getSELLER_PROCESS().getCount());
                                        break;
                                    case WAIT_COMMENT:
                                        taskStateSection.t.setTaskTaskCount(payTaskCount.getWAIT_COMMENT().getCount());
                                        break;
                                    case APPEAL:
                                        taskStateSection.t.setTaskTaskCount(payTaskCount.getAPPEAL().getCount());
                                        break;
                                    case FINISHED:
                                        taskStateSection.t.setTaskTaskCount(payTaskCount.getFINISHED().getCount());
                                        break;
                                    case CANCEL:
                                        taskStateSection.t.setTaskTaskCount(payTaskCount.getCANCEL().getCount());
                                        break;
                                }
                        }
                    }
                    return Observable.just(mAdapter);
                })
                .subscribe(new ErrorHandleSubscriber<BaseQuickAdapter>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseQuickAdapter baseQuickAdapter) {
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
    }

    public void getAskTaskCount() {
        mModel.getAskTaskCount()
                .compose(RxUtils.applySchedulers(mRootView))
                .compose(RxUtils.bindToLifecycle(mRootView))
                .flatMap(askTaskCount -> {
                    for (MyTaskStateSection taskStateSection : mList) {
                        if (!taskStateSection.isHeader) {
                            if (mApplication.getString(R.string.ask_task).equals(taskStateSection.t.getTaskType()))
                                switch (taskStateSection.t.getTaskStatus()) {
                                    case WAIT:
                                        taskStateSection.t.setTaskTaskCount(askTaskCount.getWAIT_BUYER_SUBMIT());
                                        break;
                                    case SELLER_PROCESS:
                                        taskStateSection.t.setTaskTaskCount(askTaskCount.getWAIT_SELLER());
                                        break;
                                    case APPEAL:
                                        taskStateSection.t.setTaskTaskCount(askTaskCount.getAPPEAL());
                                        break;
                                    case FINISHED:
                                        taskStateSection.t.setTaskTaskCount(askTaskCount.getFINISHED());
                                        break;
                                    case CANCEL:
                                        taskStateSection.t.setTaskTaskCount(askTaskCount.getCANCEL());
                                        break;
                                }
                        }
                    }
                    return Observable.just(mAdapter);
                })
                .subscribe(new ErrorHandleSubscriber<BaseQuickAdapter>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseQuickAdapter baseQuickAdapter) {
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
        this.mAdapter = null;
        this.mList = null;
    }

}