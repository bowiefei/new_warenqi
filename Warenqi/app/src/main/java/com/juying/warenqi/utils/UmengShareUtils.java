package com.juying.warenqi.utils;

import android.app.Activity;
import android.content.Context;

import com.blankj.utilcode.util.SPUtils;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.model.api.Api;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/29 14:01
 * </pre>
 */
public class UmengShareUtils {
    private Context mContext;
    private final SHARE_MEDIA[] mDisplayList = new SHARE_MEDIA[]{
            SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.QQ,
            SHARE_MEDIA.QZONE, SHARE_MEDIA.SINA};

    public UmengShareUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void inviteBuyerShare() {
        //多个分享
        new ShareAction((Activity) mContext).setDisplayList(mDisplayList)
                .setShareboardclickCallback((snsPlatform, share_media) -> {
                    UMWeb web = new UMWeb(Api.INVITE_BUYER + SPUtils.getInstance().getInt("userId"));
                    String appName = mContext.getResources().getString(R.string.app_name);
                    web.setDescription("随时随地,轻松赚钱!我在"+ appName +"等你哟!");
                    web.setTitle("点击下载"+ appName +"APP");
                    web.setThumb(new UMImage(mContext, R.drawable.ic_activity));
                    new ShareAction((Activity) mContext).withMedia(web)
                            .setPlatform(share_media)
                            .share();
                })
                .open();
    }

    public void inviteSellerShare() {
        //多个分享
        new ShareAction((Activity) mContext).setDisplayList(mDisplayList)
                .setShareboardclickCallback((snsPlatform, share_media) -> {
                    UMWeb web = new UMWeb(Api.INVITE_SELLER + SPUtils.getInstance().getInt("userId"));
                    String appName = mContext.getResources().getString(R.string.app_name);
                    web.setDescription("随时随地,轻松赚钱!我在"+ appName +"等你哟!");
                    web.setTitle(appName +"商家注册");
                    web.setThumb(new UMImage(mContext, R.drawable.ic_activity));
                    new ShareAction((Activity) mContext).withMedia(web)
                            .setPlatform(share_media)
                            .share();
                })
                .open();
    }

}
