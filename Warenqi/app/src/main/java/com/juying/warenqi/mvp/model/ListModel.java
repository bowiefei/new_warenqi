package com.juying.warenqi.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;
import com.juying.warenqi.mvp.contract.ListContract;
import com.juying.warenqi.mvp.model.api.service.CommonService;
import com.juying.warenqi.mvp.model.api.service.ListService;
import com.juying.warenqi.mvp.model.entity.AccountAuditInfo;
import com.juying.warenqi.mvp.model.entity.AccountInfo;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.TaobaoAccount;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;


@ActivityScope
public class ListModel extends BaseModel implements ListContract.Model {
    private Gson mGson;
    private Application mApplication;

    @Inject
    public ListModel(IRepositoryManager repositoryManager, Gson gson, Application application) {
        super(repositoryManager);
        this.mGson = gson;
        this.mApplication = application;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<BaseBean> logout() {
        return mRepositoryManager
                .obtainRetrofitService(ListService.class)
                .logout();
    }

    @Override
    public Observable<AccountInfo> getPersonalProfile() {
        return mRepositoryManager
                .obtainRetrofitService(CommonService.class)
                .getAccountInfo()
                .map(BaseBean::getResults);
    }

    @Override
    public Observable<List<TaobaoAccount>> getAccountList() {
        return mRepositoryManager
                .obtainRetrofitService(CommonService.class)
                .getAccountList("TAOBAO", false)
                .map(BaseBean::getResults);
    }

    @Override
    public Observable<BaseBean<AccountAuditInfo>> getAccountAuditInfo() {
        return mRepositoryManager
                .obtainRetrofitService(ListService.class)
                .getAccountAuditInfo();
    }

    @Override
    public Observable<BaseBean> submitAuditInfo(String name, String idcard,
                                                String idcardImg, String handImg) {
        return mRepositoryManager.obtainRetrofitService(ListService.class)
                .submitAuditInfo(name, idcard, idcardImg, handImg);
    }
}