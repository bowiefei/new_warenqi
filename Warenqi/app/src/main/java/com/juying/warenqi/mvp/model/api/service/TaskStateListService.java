package com.juying.warenqi.mvp.model.api.service;

import com.juying.warenqi.mvp.model.entity.AskTaskState;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.FlowTaskState;
import com.juying.warenqi.mvp.model.entity.PayTaskState;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/2 14:07
 * </pre>
 */
public interface TaskStateListService {
    @GET("user/subtask/listBuyerTasksFlowNew")
    Observable<BaseBean<List<FlowTaskState>>> getFlowTaskList(@Query("vo.pageNo") int pageNo,
                                                              @Query("vo.pageSize") int pageSize,
                                                              @Query("vo.searchStatus") String status,
                                                              @Nullable
                                                              @Query("vo.id") Long id);

    @GET("app/sdsub/findDaysSDTaskByStatus")
    Observable<BaseBean<List<PayTaskState>>> getPayTaskList(@Query("vo.pageNo") int pageNo,
                                                            @Query("vo.pageSize") int pageSize,
                                                            @Query("vo.searchStatus") String status,
                                                            @Nullable
                                                            @Query("vo.id") Long id);

    @GET("app/buyer/listQuestionTask")
    Observable<BaseBean<List<AskTaskState>>> getAskTaskList(@Query("vo.pageNo") int pageNo,
                                                            @Query("vo.pageSize") int pageSize,
                                                            @Query("vo.searchStatus") String status,
                                                            @Nullable
                                                            @Query("vo.id") Long id);
}
