package com.juying.warenqi.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/3 14:46
 * </pre>
 */
public class TimeUtils {
    /**
     * 判断现在的时间是否是大于今天某小时
     *
     * @param hour
     * @return
     */
    public static boolean isAfterTime(int hour) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date startDate = new Date(cal.getTimeInMillis());
        Date date = new Date();
        return date.after(startDate);
    }

    /**
     * 当前时间加天数
     *
     * @param time 之前的时间
     * @param day  要加的天数
     * @return
     */
    public static long addDay(long time, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, day);
        return calendar.getTimeInMillis();
    }
}
