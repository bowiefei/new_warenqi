package com.juying.warenqi.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;
import com.juying.warenqi.app.Constants;
import com.juying.warenqi.mvp.contract.TaskStateListContract;
import com.juying.warenqi.mvp.model.api.cache.CommonCache;
import com.juying.warenqi.mvp.model.api.service.TaskStateListService;
import com.juying.warenqi.mvp.model.entity.AskTaskState;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.FlowTaskState;
import com.juying.warenqi.mvp.model.entity.PayTaskState;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.rx_cache2.DynamicKey;
import io.rx_cache2.EvictDynamicKey;
import io.rx_cache2.Reply;


@ActivityScope
public class TaskStateListModel extends BaseModel implements TaskStateListContract.Model {
    private Gson mGson;
    private Application mApplication;

    @Inject
    public TaskStateListModel(IRepositoryManager repositoryManager, Gson gson, Application application) {
        super(repositoryManager);
        this.mGson = gson;
        this.mApplication = application;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<List<FlowTaskState>> getFlowTaskStateList(String state, int pageNo,
                                                                Long id, long idLastQueried,
                                                                boolean isUpdate) {
        Observable<List<FlowTaskState>> flowTaskStateObservable = mRepositoryManager.obtainRetrofitService(TaskStateListService.class)
                .getFlowTaskList(pageNo, Constants.LIST_PAGE_SIZE, state, id)
                .map(BaseBean::getResults);
        return mRepositoryManager.obtainCacheService(CommonCache.class)
                .getFlowTaskStateList(flowTaskStateObservable, new DynamicKey(idLastQueried)
                        , new EvictDynamicKey(isUpdate))
                .flatMap(new Function<Reply<List<FlowTaskState>>, ObservableSource<List<FlowTaskState>>>() {
                    @Override
                    public ObservableSource<List<FlowTaskState>> apply(@NonNull Reply<List<FlowTaskState>> listReply) throws Exception {
                        return Observable.just(listReply.getData());
                    }
                });
    }

    @Override
    public Observable<List<PayTaskState>> getPayTaskStateList(String state, int pageNo,
                                                              Long id, long idLastQueried,
                                                              boolean isUpdate) {
        Observable<List<PayTaskState>> payTaskStateObservable = mRepositoryManager.obtainRetrofitService(TaskStateListService.class)
                .getPayTaskList(pageNo, Constants.LIST_PAGE_SIZE, state, id)
                .map(BaseBean::getResults);
        return mRepositoryManager.obtainCacheService(CommonCache.class)
                .getPayTaskStateList(payTaskStateObservable, new DynamicKey(idLastQueried)
                        , new EvictDynamicKey(isUpdate))
                .flatMap(new Function<Reply<List<PayTaskState>>, ObservableSource<List<PayTaskState>>>() {
                    @Override
                    public ObservableSource<List<PayTaskState>> apply(@NonNull Reply<List<PayTaskState>> listReply) throws Exception {
                        return Observable.just(listReply.getData());
                    }
                });
    }

    @Override
    public Observable<List<AskTaskState>> getAskTaskStateList(String state, int pageNo, Long id, long idLastQueried, boolean isUpdate) {
        Observable<List<AskTaskState>> askTaskStateObservable = mRepositoryManager.obtainRetrofitService(TaskStateListService.class)
                .getAskTaskList(pageNo, Constants.LIST_PAGE_SIZE, state, id)
                .map(BaseBean::getResults);
        return mRepositoryManager.obtainCacheService(CommonCache.class)
                .getAskTaskStateList(askTaskStateObservable, new DynamicKey(idLastQueried)
                        , new EvictDynamicKey(isUpdate))
                .flatMap(new Function<Reply<List<AskTaskState>>, ObservableSource<List<AskTaskState>>>() {
                    @Override
                    public ObservableSource<List<AskTaskState>> apply(@NonNull Reply<List<AskTaskState>> listReply) throws Exception {
                        return Observable.just(listReply.getData());
                    }
                });
    }
}