package com.juying.warenqi.mvp.ui.adapter;

import android.text.InputType;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.SizeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.model.entity.AccountAuditMultiItem;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/5 16:22
 * </pre>
 */
public class MultiItemAdapter extends BaseMultiItemQuickAdapter<AccountAuditMultiItem, BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public MultiItemAdapter(List<AccountAuditMultiItem> data) {
        super(data);
        addItemType(AccountAuditMultiItem.TEXT, R.layout.item_edittext);
        addItemType(AccountAuditMultiItem.EDITTEXT, R.layout.item_edittext);
        addItemType(AccountAuditMultiItem.IMG, R.layout.item_account_audit_img);
    }

    @Override
    protected void convert(BaseViewHolder helper, AccountAuditMultiItem item) {
        EditText editText = helper.getView(R.id.et_input_text);
        switch (helper.getItemViewType()) {
            case AccountAuditMultiItem.TEXT:
                editText.setTextColor(mContext
                        .getApplicationContext().getResources().getColor(R.color.grey_333));
                editText.setEnabled(false);
                helper.setText(R.id.tv_edit_title, item.getTitle() + "：");
                editText.setText(item.getContent());
                break;
            case AccountAuditMultiItem.EDITTEXT:
                helper.setText(R.id.tv_edit_title, item.getTitle() + "：");

                switch (item.getStatus()) {
                    case 0:
                        editText.setEnabled(true);
                        editText.setHint(item.getContent());
                        break;
                    case 1:
                        editText.setEnabled(false);
                        editText.setText(item.getContent());
                        break;
                    case 2:
                        editText.setEnabled(true);
                        editText.setText(item.getContent());
                        break;
                }

                if (item.getTitle().equals("真实姓名")) {
                    editText.setInputType(InputType.TYPE_CLASS_TEXT);
                } else if (item.getTitle().equals("证件号码")) {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                }

                if (editText.isEnabled() && TextUtils.isEmpty(editText.getText()))
                    RxTextView.textChanges(editText)
                            .observeOn(Schedulers.io())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe(charSequence -> item.setContent(charSequence.toString()));
                break;
            case AccountAuditMultiItem.IMG:
                helper.setText(R.id.tv_audit_title, item.getTitle() + "：")
                        .setText(R.id.tv_audit_result, item.getContent())
                        .setVisible(R.id.tv_audit_result, !TextUtils.isEmpty(item.getContent()))
                        .setVisible(R.id.tv_card_notice1, TextUtils.isEmpty(item.getImgUrl1()))
                        .setVisible(R.id.tv_card_notice2, TextUtils.isEmpty(item.getImgUrl2()));
                switch (item.getStatus()) {
                    case 0:
                    case 2:
                        helper.addOnClickListener(R.id.rl_img1)
                        .addOnClickListener(R.id.rl_img2);
                        break;
                    case 1:
                        helper.setTextColor(R.id.tv_audit_result, mContext.getResources()
                                .getColor(R.color.green_4ab69f));
                        break;
                }

                if (!TextUtils.isEmpty(item.getImgUrl1())) {
                    helper.getView(R.id.rl_img1).setBackgroundDrawable(null);
                }

                if (!TextUtils.isEmpty(item.getImgUrl2())) {
                    helper.getView(R.id.rl_img2).setBackgroundDrawable(null);
                }

                Glide.with(mContext)
                        .load(item.getImgUrl1())
                        .bitmapTransform(new CenterCrop(mContext),
                                new RoundedCornersTransformation(mContext, SizeUtils.dp2px(10), 0))
                        .into((ImageView) helper.getView(R.id.iv_audit_img1));

                Glide.with(mContext)
                        .load(item.getImgUrl2())
                        .bitmapTransform(new CenterCrop(mContext),
                                new RoundedCornersTransformation(mContext, SizeUtils.dp2px(10), 0))
                        .into((ImageView) helper.getView(R.id.iv_audit_img2));
                break;
        }
    }
}
