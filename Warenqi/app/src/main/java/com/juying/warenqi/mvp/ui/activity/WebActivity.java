package com.juying.warenqi.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.BarUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.just.library.AgentWeb;
import com.just.library.ChromeClientCallbackManager;
import com.juying.warenqi.R;
import com.juying.warenqi.utils.UmengShareUtils;
import com.juying.warenqi.view.popupwindows.BottomSlideListPop;
import com.juying.warenqi.view.popupwindows.PopTitle;
import com.zhy.autolayout.AutoLinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/29 10:02
 * </pre>
 */
public class WebActivity extends AppCompatActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.web_container)
    AutoLinearLayout webContainer;
    @BindView(R.id.toolbar_right_iv)
    ImageView toolbarRightIv;
    private AgentWeb mAgentWeb;
    private List<String> mWebTitles = new ArrayList<>();
    private List<PopTitle> mPopTitles = new ArrayList<>();
    private int shownIconRes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);

        BarUtils.setTranslucent(this);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        String title = intent.getStringExtra("title");
        shownIconRes = intent.getIntExtra("shownIconRes", 0);
        if (shownIconRes != 0) {
            toolbarRightIv.setVisibility(View.VISIBLE);
            toolbarRightIv.setImageResource(shownIconRes);
        }

        mAgentWeb = AgentWeb.with(this)//传入Activity
                .setAgentWebParent(webContainer, new LinearLayout.LayoutParams(-1, -1))//传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams
                .useDefaultIndicator()//使用默认进度条
                .defaultProgressBarColor()// 使用默认进度条颜色
                .setReceivedTitleCallback(mCallback)//设置 Web 页面的 title 回调
                .createAgentWeb()
                .ready()
                .go(url);

        if (TextUtils.isEmpty(toolbarTitle.getText())) {
            toolbarTitle.setText(title);
        }
    }

    private ChromeClientCallbackManager.ReceivedTitleCallback mCallback = new ChromeClientCallbackManager.ReceivedTitleCallback() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            mWebTitles.add(title);
            toolbarTitle.setText(title);
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
            if (mWebTitles.size() > 1) {
                mWebTitles.remove(mWebTitles.size() - 1);
                toolbarTitle.setText(mWebTitles.get(mWebTitles.size() - 1));
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAgentWeb.getWebLifeCycle().onDestroy();
        mPopTitles = null;
        mWebTitles = null;
    }

    @OnClick({R.id.toolbar_back, R.id.toolbar_right_iv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                onBackPressed();
                break;
            case R.id.toolbar_right_iv:
                switch (shownIconRes) {
                    case R.drawable.umeng_share:
                        showInvitePop();
                        break;
                    case R.drawable.kefu:
                        break;
                }
                break;
        }
    }

    private void showInvitePop() {
        mPopTitles.clear();
        PopTitle popTitle1 = new PopTitle();
        popTitle1.setTitle("请选择");
        popTitle1.setTextSize(12);
        popTitle1.setTextColor(R.color.grey_333);
        PopTitle popTitle2 = new PopTitle();
        popTitle2.setTitle("邀请买手");
        popTitle2.setTextSize(14);
        popTitle2.setTextColor(R.color.blue_4680fe);
        PopTitle popTitle3 = new PopTitle();
        popTitle3.setTitle("邀请商家");
        popTitle3.setTextSize(14);
        popTitle3.setTextColor(R.color.blue_4680fe);
        PopTitle popTitle4 = new PopTitle();
        popTitle4.setTitle("取消");
        popTitle4.setTextSize(12);
        popTitle4.setTextColor(R.color.red_fb607f);
        mPopTitles.add(popTitle1);
        mPopTitles.add(popTitle2);
        mPopTitles.add(popTitle3);
        mPopTitles.add(popTitle4);
        BottomSlideListPop listPop = new BottomSlideListPop(this, mPopTitles);
        listPop.setPopListClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                PopTitle item = (PopTitle) adapter.getItem(position);
                String title = item.getTitle();
                UmengShareUtils umengShareUtils = new UmengShareUtils(WebActivity.this);
                switch (title) {
                    case "邀请买手":
                        umengShareUtils.inviteBuyerShare();
                        break;
                    case "邀请商家":
                        umengShareUtils.inviteSellerShare();
                        break;
                }
                if (!"请选择".equals(title)) {
                    listPop.dismiss();
                }
            }
        });
        listPop.showPopupWindow();
    }
}
