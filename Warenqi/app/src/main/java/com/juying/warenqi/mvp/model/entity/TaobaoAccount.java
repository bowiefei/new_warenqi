package com.juying.warenqi.mvp.model.entity;

import com.juying.warenqi.enums.AccountStatus;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/6/29 21:36
 * </pre>
 */
public class TaobaoAccount {
    private String age;
    private String address;
    private String commdityType;
    private String credit;
    private String consignee;
    private String city;
    private String gender;
    private boolean hasTask;
    private boolean hasTaskExecuting;
    private boolean huabei;
    private String huabeiImg;
    private AccountStatus huabeiExamine;
    private long id;
    private int lockedDays;
    private int mouthTakenNum;
    private String mobile;
    private String memo;
    private String nick;
    private int orderNumber;
    private String orderNoTest;
    private int receivedTaskCount;
    private String region;
    private AccountStatus status;
    private String state;
    private String taobaoPic;
    private int totalTakenNum;
    private int userId;
    private int weekTakenNum;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCommdityType() {
        return commdityType;
    }

    public void setCommdityType(String commdityType) {
        this.commdityType = commdityType;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isHasTask() {
        return hasTask;
    }

    public void setHasTask(boolean hasTask) {
        this.hasTask = hasTask;
    }

    public boolean isHasTaskExecuting() {
        return hasTaskExecuting;
    }

    public void setHasTaskExecuting(boolean hasTaskExecuting) {
        this.hasTaskExecuting = hasTaskExecuting;
    }

    public boolean isHuabei() {
        return huabei;
    }

    public void setHuabei(boolean huabei) {
        this.huabei = huabei;
    }

    public String getHuabeiImg() {
        return huabeiImg;
    }

    public void setHuabeiImg(String huabeiImg) {
        this.huabeiImg = huabeiImg;
    }

    public AccountStatus getHuabeiExamine() {
        return huabeiExamine;
    }

    public void setHuabeiExamine(AccountStatus huabeiExamine) {
        this.huabeiExamine = huabeiExamine;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getLockedDays() {
        return lockedDays;
    }

    public void setLockedDays(int lockedDays) {
        this.lockedDays = lockedDays;
    }

    public int getMouthTakenNum() {
        return mouthTakenNum;
    }

    public void setMouthTakenNum(int mouthTakenNum) {
        this.mouthTakenNum = mouthTakenNum;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderNoTest() {
        return orderNoTest;
    }

    public void setOrderNoTest(String orderNoTest) {
        this.orderNoTest = orderNoTest;
    }

    public int getReceivedTaskCount() {
        return receivedTaskCount;
    }

    public void setReceivedTaskCount(int receivedTaskCount) {
        this.receivedTaskCount = receivedTaskCount;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTaobaoPic() {
        return taobaoPic;
    }

    public void setTaobaoPic(String taobaoPic) {
        this.taobaoPic = taobaoPic;
    }

    public int getTotalTakenNum() {
        return totalTakenNum;
    }

    public void setTotalTakenNum(int totalTakenNum) {
        this.totalTakenNum = totalTakenNum;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getWeekTakenNum() {
        return weekTakenNum;
    }

    public void setWeekTakenNum(int weekTakenNum) {
        this.weekTakenNum = weekTakenNum;
    }
}
