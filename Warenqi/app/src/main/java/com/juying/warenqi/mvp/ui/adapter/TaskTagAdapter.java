package com.juying.warenqi.mvp.ui.adapter;

import android.support.annotation.Nullable;
import android.widget.TextView;

import com.blankj.utilcode.util.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.model.entity.TaskTag;
import com.juying.warenqi.mvp.ui.holder.BaseAutoLayoutHolder;

import java.util.List;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/1 15:05
 * </pre>
 */
public class TaskTagAdapter extends BaseQuickAdapter<TaskTag, BaseAutoLayoutHolder> {
    public TaskTagAdapter(@Nullable List<TaskTag> data) {
        super(R.layout.item_textview, data);
    }

    @Override
    protected void convert(BaseAutoLayoutHolder helper, TaskTag item) {
        TextView textView = helper.getView(R.id.tv);
        if (item.getTextSize() > 0) textView.setTextSize(item.getTextSize());
        else textView.setTextSize(12);
        int padding = item.getPadding();
        if (padding > 0) textView.setPadding(padding, padding, padding, padding);
        else {
            int dp4 = SizeUtils.dp2px(4);
            textView.setPadding(dp4, dp4, dp4, dp4);
        }
        helper.setText(R.id.tv, item.getTitle())
                .setTextColor(R.id.tv, mContext.getApplicationContext()
                        .getResources().getColor(item.getTextColor()))
                .setBackgroundRes(R.id.tv, item.getBackgroundRes());
    }
}
