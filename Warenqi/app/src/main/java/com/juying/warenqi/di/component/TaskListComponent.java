package com.juying.warenqi.di.component;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.juying.warenqi.di.module.TaskListModule;

import com.juying.warenqi.mvp.ui.activity.TaskListActivity;

@ActivityScope
@Component(modules = TaskListModule.class, dependencies = AppComponent.class)
public interface TaskListComponent {
    void inject(TaskListActivity activity);
}