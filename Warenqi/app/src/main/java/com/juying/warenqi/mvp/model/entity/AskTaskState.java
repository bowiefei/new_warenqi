package com.juying.warenqi.mvp.model.entity;

import com.juying.warenqi.enums.TaskStatus;

import java.util.List;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/3 10:17
 * </pre>
 */
public class AskTaskState {
    private String accountImg;
    private String afterCommentImg;
    private int afterCommentProfit;
    private int afterCommentIngot;
    private String answer;
    private String applayImg;
    private long buyerAccountId;
    private String buyerAccountNick;
    private long buyerId;
    private String buyerNick;
    private boolean chat;
    private String chatImg;
    private boolean comment;
    private String commentImg;
    private String commentWord;
    private long commentTime;
    private int comssionProfit;
    private boolean coupon;
    private long createTime;
    private boolean delay;
    private String detailImg;
    private long endTime;
    private long examineCommentTime;
    private int exportCount;
    private int expressIngot;
    private int expressProfit;
    private int extraProfit;
    private String expressType;
    private long finishTime;
    private int flowId;
    private boolean huabei;
    private boolean huobi;
    private long id;
    private int ingot;
    private int itemBuyNum;
    private long itemId;
    private String itemPicUrl;
    private long itemPlanId;
    private int itemPrice;
    private String itemSubwayPicUrl;
    private String itemTitle;
    private String itemUrl;
    private List<?> items;
    private String logisticsImg;
    private String message;
    private String memo;
    private String orderId;
    private String orderImg;
    private int pageNo;
    private int pageSize;
    private int paidFee;
    private long payTime;
    private long planId;
    private String preBuyerAccount;
    private int questionCount;
    private String questionImg;
    private int questionIngot;
    private int questionProfit;
    private int refoundProfit;
    private int sdTaskId;
    private String sdType;
    private String searchImg;
    private int sellerId;
    private String sellerNick;
    private long shopId;
    private String shopName;
    private String sku;
    private int startIndex;
    private long startTime;
    private TaskStatus status;
    private long submitTime;
    private long takeTime;
    private boolean taojinbi;
    private int taskId;
    private String taskRequest;
    private int totalCount;
    private int totalPageCount;
    private String type;
    private long updateTime;
    private boolean waitConfirm;

    public String getAccountImg() {
        return accountImg;
    }

    public void setAccountImg(String accountImg) {
        this.accountImg = accountImg;
    }

    public int getAfterCommentProfit() {
        return afterCommentProfit;
    }

    public void setAfterCommentProfit(int afterCommentProfit) {
        this.afterCommentProfit = afterCommentProfit;
    }

    public long getBuyerAccountId() {
        return buyerAccountId;
    }

    public void setBuyerAccountId(long buyerAccountId) {
        this.buyerAccountId = buyerAccountId;
    }

    public String getBuyerAccountNick() {
        return buyerAccountNick;
    }

    public void setBuyerAccountNick(String buyerAccountNick) {
        this.buyerAccountNick = buyerAccountNick;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerNick() {
        return buyerNick;
    }

    public void setBuyerNick(String buyerNick) {
        this.buyerNick = buyerNick;
    }

    public boolean isChat() {
        return chat;
    }

    public void setChat(boolean chat) {
        this.chat = chat;
    }

    public String getChatImg() {
        return chatImg;
    }

    public void setChatImg(String chatImg) {
        this.chatImg = chatImg;
    }

    public boolean isComment() {
        return comment;
    }

    public void setComment(boolean comment) {
        this.comment = comment;
    }

    public String getCommentImg() {
        return commentImg;
    }

    public void setCommentImg(String commentImg) {
        this.commentImg = commentImg;
    }

    public long getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(long commentTime) {
        this.commentTime = commentTime;
    }

    public int getComssionProfit() {
        return comssionProfit;
    }

    public void setComssionProfit(int comssionProfit) {
        this.comssionProfit = comssionProfit;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public boolean isDelay() {
        return delay;
    }

    public void setDelay(boolean delay) {
        this.delay = delay;
    }

    public String getDetailImg() {
        return detailImg;
    }

    public void setDetailImg(String detailImg) {
        this.detailImg = detailImg;
    }

    public long getExamineCommentTime() {
        return examineCommentTime;
    }

    public void setExamineCommentTime(long examineCommentTime) {
        this.examineCommentTime = examineCommentTime;
    }

    public int getExportCount() {
        return exportCount;
    }

    public void setExportCount(int exportCount) {
        this.exportCount = exportCount;
    }

    public int getExpressIngot() {
        return expressIngot;
    }

    public void setExpressIngot(int expressIngot) {
        this.expressIngot = expressIngot;
    }

    public int getExpressProfit() {
        return expressProfit;
    }

    public void setExpressProfit(int expressProfit) {
        this.expressProfit = expressProfit;
    }

    public String getExpressType() {
        return expressType;
    }

    public void setExpressType(String expressType) {
        this.expressType = expressType;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    public int getFlowId() {
        return flowId;
    }

    public void setFlowId(int flowId) {
        this.flowId = flowId;
    }

    public boolean isHuabei() {
        return huabei;
    }

    public void setHuabei(boolean huabei) {
        this.huabei = huabei;
    }

    public boolean isHuobi() {
        return huobi;
    }

    public void setHuobi(boolean huobi) {
        this.huobi = huobi;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIngot() {
        return ingot;
    }

    public void setIngot(int ingot) {
        this.ingot = ingot;
    }

    public int getItemBuyNum() {
        return itemBuyNum;
    }

    public void setItemBuyNum(int itemBuyNum) {
        this.itemBuyNum = itemBuyNum;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public long getItemPlanId() {
        return itemPlanId;
    }

    public void setItemPlanId(long itemPlanId) {
        this.itemPlanId = itemPlanId;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemSubwayPicUrl() {
        return itemSubwayPicUrl;
    }

    public void setItemSubwayPicUrl(String itemSubwayPicUrl) {
        this.itemSubwayPicUrl = itemSubwayPicUrl;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public String getLogisticsImg() {
        return logisticsImg;
    }

    public void setLogisticsImg(String logisticsImg) {
        this.logisticsImg = logisticsImg;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderImg() {
        return orderImg;
    }

    public void setOrderImg(String orderImg) {
        this.orderImg = orderImg;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPaidFee() {
        return paidFee;
    }

    public void setPaidFee(int paidFee) {
        this.paidFee = paidFee;
    }

    public long getPayTime() {
        return payTime;
    }

    public void setPayTime(long payTime) {
        this.payTime = payTime;
    }

    public String getPreBuyerAccount() {
        return preBuyerAccount;
    }

    public void setPreBuyerAccount(String preBuyerAccount) {
        this.preBuyerAccount = preBuyerAccount;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(int questionCount) {
        this.questionCount = questionCount;
    }

    public int getRefoundProfit() {
        return refoundProfit;
    }

    public void setRefoundProfit(int refoundProfit) {
        this.refoundProfit = refoundProfit;
    }

    public int getSdTaskId() {
        return sdTaskId;
    }

    public void setSdTaskId(int sdTaskId) {
        this.sdTaskId = sdTaskId;
    }

    public String getSdType() {
        return sdType;
    }

    public void setSdType(String sdType) {
        this.sdType = sdType;
    }

    public String getSearchImg() {
        return searchImg;
    }

    public void setSearchImg(String searchImg) {
        this.searchImg = searchImg;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerNick() {
        return sellerNick;
    }

    public void setSellerNick(String sellerNick) {
        this.sellerNick = sellerNick;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public long getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(long submitTime) {
        this.submitTime = submitTime;
    }

    public long getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(long takeTime) {
        this.takeTime = takeTime;
    }

    public boolean isTaojinbi() {
        return taojinbi;
    }

    public void setTaojinbi(boolean taojinbi) {
        this.taojinbi = taojinbi;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskRequest() {
        return taskRequest;
    }

    public void setTaskRequest(String taskRequest) {
        this.taskRequest = taskRequest;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isWaitConfirm() {
        return waitConfirm;
    }

    public void setWaitConfirm(boolean waitConfirm) {
        this.waitConfirm = waitConfirm;
    }

    public List<?> getItems() {
        return items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }

    public String getAfterCommentImg() {
        return afterCommentImg;
    }

    public void setAfterCommentImg(String afterCommentImg) {
        this.afterCommentImg = afterCommentImg;
    }

    public int getAfterCommentIngot() {
        return afterCommentIngot;
    }

    public void setAfterCommentIngot(int afterCommentIngot) {
        this.afterCommentIngot = afterCommentIngot;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getApplayImg() {
        return applayImg;
    }

    public void setApplayImg(String applayImg) {
        this.applayImg = applayImg;
    }

    public String getCommentWord() {
        return commentWord;
    }

    public void setCommentWord(String commentWord) {
        this.commentWord = commentWord;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getExtraProfit() {
        return extraProfit;
    }

    public void setExtraProfit(int extraProfit) {
        this.extraProfit = extraProfit;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public String getQuestionImg() {
        return questionImg;
    }

    public void setQuestionImg(String questionImg) {
        this.questionImg = questionImg;
    }

    public int getQuestionIngot() {
        return questionIngot;
    }

    public void setQuestionIngot(int questionIngot) {
        this.questionIngot = questionIngot;
    }

    public int getQuestionProfit() {
        return questionProfit;
    }

    public void setQuestionProfit(int questionProfit) {
        this.questionProfit = questionProfit;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
