package com.juying.warenqi.app;

import com.flyco.tablayout.listener.CustomTabEntity;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/6/29 22:06
 * </pre>
 */
public class AccountTabEntity implements CustomTabEntity {

    private String accountNick;
    private long accountId;
    private String accountStatus;

    public AccountTabEntity(String accountNick, long accountId, String accountStatus) {
        this.accountNick = accountNick;
        this.accountId = accountId;
        this.accountStatus = accountStatus;
    }

    @Override
    public String getTabTitle() {
        return accountNick;
    }

    @Override
    public int getTabSelectedIcon() {
        return 0;
    }

    @Override
    public int getTabUnselectedIcon() {
        return 0;
    }

    public long getAccountId() {
        return accountId;
    }

    public String getAccountStatus() {
        return accountStatus;
    }
}
