package com.juying.warenqi.mvp.presenter;

import android.app.Application;

import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.widget.imageloader.ImageLoader;
import com.juying.warenqi.enums.TaskTypes;
import com.juying.warenqi.mvp.contract.TaskListContract;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.FlowTaskList;
import com.juying.warenqi.mvp.model.entity.PayTaskList;
import com.juying.warenqi.mvp.model.entity.TaobaoAccount;
import com.juying.warenqi.mvp.ui.adapter.FlowTaskAdapter;
import com.juying.warenqi.mvp.ui.adapter.PayTaskAdapter;
import com.juying.warenqi.utils.RxUtils;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class TaskListPresenter extends BasePresenter<TaskListContract.Model, TaskListContract.View> {
    private RxErrorHandler mErrorHandler;
    private Application mApplication;
    private ImageLoader mImageLoader;
    private AppManager mAppManager;
    private FlowTaskAdapter mFlowTaskAdapter;
    private List<FlowTaskList> mFlowTaskLists = new ArrayList<>();
    private PayTaskAdapter mPayTaskAdapter;
    private List<PayTaskList> mPayTaskLists = new ArrayList<>();
    private boolean isFirst = true;
    private long idLastQueried = 1;

    @Inject
    public TaskListPresenter(TaskListContract.Model model, TaskListContract.View rootView
            , RxErrorHandler handler, Application application
            , ImageLoader imageLoader, AppManager appManager) {
        super(model, rootView);
        this.mErrorHandler = handler;
        this.mApplication = application;
        this.mImageLoader = imageLoader;
        this.mAppManager = appManager;
    }

    public void getAccountList() {
        mModel.getAccountList()
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<List<TaobaoAccount>>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull List<TaobaoAccount> taobaoAccounts) {
                        mRootView.setAccountTab(taobaoAccounts);
                    }
                });
    }

    public void getFlowTaskList(int pageNo, @TaskTypes String taskType, long buyerAccountId, boolean isUpdate) {
        if (mFlowTaskAdapter == null) {
            mFlowTaskAdapter = new FlowTaskAdapter(mFlowTaskLists);
            mRootView.setAdapter(mFlowTaskAdapter);
        }

        if (isUpdate) idLastQueried = 1;

        boolean isEvictCache = isUpdate;//是否驱逐缓存,为true即不使用缓存,每次下拉刷新即需要最新数据,则不使用缓存

        if (isUpdate && isFirst) {//默认在第一次上拉刷新时使用缓存
            isFirst = false;
            isEvictCache = false;
        }

        mModel.getFlowTaskList(pageNo, taskType, buyerAccountId, idLastQueried, isEvictCache)
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<List<FlowTaskList>>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull List<FlowTaskList> flowTaskLists) {
                        Logger.d(flowTaskLists);
                        if (flowTaskLists.size() > 0)
                            idLastQueried = flowTaskLists.get(flowTaskLists.size() - 1).getId();//记录最后一个id,用于下一次请求
                        mFlowTaskAdapter.removeAllFooterView();
                        if (isUpdate) {
                            mFlowTaskAdapter.setNewData(flowTaskLists);
                        } else {
                            mFlowTaskAdapter.addData(flowTaskLists);
                            if (flowTaskLists.size() == 0) {
                                mRootView.setNoMoreData(mFlowTaskAdapter);
                                mFlowTaskAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    public void getPayTaskList(int pageNo, @TaskTypes String taskType, long buyerAccountId, boolean isUpdate) {
        if (mPayTaskAdapter == null) {
            mPayTaskAdapter = new PayTaskAdapter(mPayTaskLists);
            mRootView.setAdapter(mPayTaskAdapter);
            Logger.d(mPayTaskLists.size());
        }

        mModel.getPayTaskList(pageNo, taskType, buyerAccountId, isUpdate)
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<BaseBean<List<PayTaskList>>>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseBean<List<PayTaskList>> payTaskLists) {
                        Logger.d(payTaskLists);
                        if (payTaskLists.getResults() != null) {
                            mPayTaskAdapter.removeAllFooterView();
                            if (isUpdate) {
                                mPayTaskAdapter.setNewData(payTaskLists.getResults());
                            } else {
                                mPayTaskAdapter.addData(payTaskLists.getResults());
                                if (payTaskLists.getResults().size() == 0) {
                                    mRootView.setNoMoreData(mPayTaskAdapter);
                                }
                            }
                        } else {
                            mPayTaskAdapter.setNewData(null);
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
        mFlowTaskAdapter = null;
        mPayTaskAdapter = null;
        mFlowTaskLists = null;
        mPayTaskLists = null;
    }

}