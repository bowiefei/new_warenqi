package com.juying.warenqi.mvp.model.api.cache;

import com.juying.warenqi.mvp.model.entity.AskTaskState;
import com.juying.warenqi.mvp.model.entity.FlowTaskList;
import com.juying.warenqi.mvp.model.entity.FlowTaskState;
import com.juying.warenqi.mvp.model.entity.PayTaskList;
import com.juying.warenqi.mvp.model.entity.PayTaskState;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.rx_cache2.DynamicKey;
import io.rx_cache2.EvictProvider;
import io.rx_cache2.LifeCache;
import io.rx_cache2.Reply;

public interface CommonCache {

    @LifeCache(duration = 1, timeUnit = TimeUnit.MINUTES)
    Observable<Reply<List<FlowTaskList>>> getFlowTaskList(Observable<List<FlowTaskList>> flowTasks,
                                                          DynamicKey idLastQueried,
                                                          EvictProvider evictProvider);

    @LifeCache(duration = 1, timeUnit = TimeUnit.MINUTES)
    Observable<Reply<List<PayTaskList>>> getPayTaskList(Observable<List<PayTaskList>> payTasks,
                                                        DynamicKey idLastQueried,
                                                        EvictProvider evictProvider);

    @LifeCache(duration = 1, timeUnit = TimeUnit.MINUTES)
    Observable<Reply<List<FlowTaskState>>> getFlowTaskStateList(Observable<List<FlowTaskState>> payTasks,
                                                                DynamicKey idLastQueried,
                                                                EvictProvider evictProvider);

    @LifeCache(duration = 1, timeUnit = TimeUnit.MINUTES)
    Observable<Reply<List<PayTaskState>>> getPayTaskStateList(Observable<List<PayTaskState>> payTasks,
                                                              DynamicKey idLastQueried,
                                                              EvictProvider evictProvider);

    @LifeCache(duration = 1, timeUnit = TimeUnit.MINUTES)
    Observable<Reply<List<AskTaskState>>> getAskTaskStateList(Observable<List<AskTaskState>> payTasks,
                                                              DynamicKey idLastQueried,
                                                              EvictProvider evictProvider);

}
