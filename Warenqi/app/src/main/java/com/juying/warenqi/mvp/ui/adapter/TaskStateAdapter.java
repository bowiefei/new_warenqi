package com.juying.warenqi.mvp.ui.adapter;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juying.warenqi.R;
import com.juying.warenqi.enums.TaskStatus;
import com.juying.warenqi.enums.TaskTypes;
import com.juying.warenqi.mvp.model.entity.AskTaskState;
import com.juying.warenqi.mvp.model.entity.FlowTaskState;
import com.juying.warenqi.mvp.model.entity.PayTaskState;
import com.juying.warenqi.mvp.model.entity.TaskTag;
import com.juying.warenqi.mvp.ui.holder.BaseAutoLayoutHolder;
import com.juying.warenqi.view.RoundedBackgroundSpan;
import com.juying.warenqi.view.countdownview.CountdownView;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/2 14:37
 * </pre>
 */
public class TaskStateAdapter<T> extends BaseQuickAdapter<T, BaseAutoLayoutHolder> {

    public TaskStateAdapter(@Nullable List<T> data) {
        super(R.layout.item_task_state, data);
    }

    @Override
    protected void convert(BaseAutoLayoutHolder helper, T item) {
        Resources resources = mContext.getApplicationContext().getResources();
        String taskId = "";
        String timeStr = "";
        String itemPicUrl = "";
        SpannableStringBuilder spannableStringBuilder = null;
        List<TaskTag> tagList = new ArrayList<>();
        if (item instanceof FlowTaskState) {
            taskId = "任务编号：" + ((FlowTaskState) item).getId();
            timeStr = TimeUtils.millis2String(((FlowTaskState) item).getTakenTime(),
                    new SimpleDateFormat("MM-dd", Locale.getDefault()));
            itemPicUrl = ((FlowTaskState) item).getTaskImage();

            spannableStringBuilder = new SpanUtils()
                    .appendImage(R.drawable.wangwang_icon, SpanUtils.ALIGN_CENTER)
                    .appendSpace(10)
                    .appendLine(((FlowTaskState) item).getBuyerAccountNick())
                    .setForegroundColor(resources.getColor(R.color.grey_999))
                    .setFontSize(13, true)
                    .appendImage(R.drawable.yongjin_icon, SpanUtils.ALIGN_TOP)
                    .appendSpace(10)
                    .appendLine(BigDecimal.valueOf(((FlowTaskState) item).getIngot()).movePointLeft(2).toPlainString())
                    .setForegroundColor(resources.getColor(R.color.green_4ab69f))
                    .setFontSize(27, true)
                    .appendLine(((FlowTaskState) item).getTaskType().getName())
                    .setForegroundColor(resources.getColor(R.color.grey_999))
                    .setFontSize(13, true).create();
            TaskStatus status = ((FlowTaskState) item).getStatus();
            tagList.clear();
            switch (status) {
                case TAKEN:
                    tagList.add(new TaskTag("进行中", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_yellow_2dp_radius_rect));
                    break;
                case WAIT_EXAMINE:
                    tagList.add(new TaskTag(status.getTitle(), 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    break;
                case PASS:
                    tagList.add(new TaskTag("待发放", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    break;
                case FINISHED:
                    tagList.add(new TaskTag(status.getTitle(), 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    break;
//                case NOPASS:
//                    tagList.add(new TaskTag("未通过", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
//                            R.drawable.shape_stroke_blue_2dp_radius_rect));
//                    break;
                case CANCEL:
                    tagList.add(new TaskTag(status.getTitle(), 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    break;
            }
        } else if (item instanceof PayTaskState) {
            taskId = "任务编号：" + ((PayTaskState) item).getId();
            timeStr = TimeUtils.millis2String(((PayTaskState) item).getTakeTime(),
                    new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()));
            itemPicUrl = ((PayTaskState) item).getItemPicUrl();

            SpanUtils spanUtils = new SpanUtils()
                    .appendImage(R.drawable.ic_alibuyer, SpanUtils.ALIGN_CENTER)
                    .appendSpace(10)
                    .appendLine(((PayTaskState) item).getBuyerAccountNick())
                    .setForegroundColor(resources.getColor(R.color.grey_333))
                    .setFontSize(13, true)
                    .append(BigDecimal.valueOf(((PayTaskState) item).getIngot()).movePointLeft(2).toPlainString())
                    .setForegroundColor(resources.getColor(R.color.yellow_ffb64d))
                    .setBold()
                    .setFontSize(27, true);

            if (((PayTaskState) item).getExtraReward() > 0) {
                spanUtils.append(" + ")
                        .setForegroundColor(resources.getColor(R.color.grey_dedede))
                        .setBold()
                        .setFontSize(25, true)
                        .append(BigDecimal.valueOf(((PayTaskState) item).getExtraReward()).movePointLeft(2).toPlainString())
                        .setForegroundColor(resources.getColor(R.color.yellow_ffb64d))
                        .setBold()
                        .setFontSize(27, true)
                        .appendSpace(SizeUtils.dp2px(10))
                        .append("加赏")
                        .setSpans(new RoundedBackgroundSpan(resources.getColor(R.color.red_fb607f),
                                resources.getColor(R.color.white), SizeUtils.sp2px(10)
                                , SizeUtils.dp2px(2)));
            }

            spannableStringBuilder =
                    spanUtils
                            .appendLine()
                            .appendLine("预付金额：" + (((PayTaskState) item).getItemPriceStr()))
                            .setForegroundColor(resources.getColor(R.color.grey_999))
                            .setFontSize(13, true).create();

            tagList.clear();

            String sdType = ((PayTaskState) item).getSdType();

            switch (sdType) {
                case TaskTypes.FIRST_DAY_BUY:
                    tagList.add(new TaskTag("今日购", 10, SizeUtils.dp2px(2), R.color.white,
                            R.drawable.shape_solid_red_2dp_radius_rect));
                    break;
                case TaskTypes.SECOND_DAY_BUY:
                    tagList.add(new TaskTag("次日购", 10, SizeUtils.dp2px(2), R.color.white,
                            R.drawable.shape_solid_red_2dp_radius_rect));
                    break;
                case TaskTypes.THIRD_DAY_BUY:
                    tagList.add(new TaskTag("第3日购", 10, SizeUtils.dp2px(2), R.color.white,
                            R.drawable.shape_solid_red_2dp_radius_rect));
                    break;
                case TaskTypes.FOURTH_DAY_BUY:
                    tagList.add(new TaskTag("第4日购", 10, SizeUtils.dp2px(2), R.color.white,
                            R.drawable.shape_solid_red_2dp_radius_rect));
                    break;
            }

            TextView btnCancel = helper.getView(R.id.btn_cancel_task);
            btnCancel.setBackgroundResource(R.drawable.shape_solid_lighter_blue_2dp_rect);
            btnCancel.setTextColor(resources.getColor(R.color.blue_4680fe));
            btnCancel.setText("任务延时");

            TextView btnStartTask = helper.getView(R.id.btn_start_task);
            btnStartTask.setBackgroundResource(R.drawable.shape_blue_2dp_radius_rect);
            btnStartTask.setTextColor(resources.getColor(R.color.white));

            TaskStatus status = ((PayTaskState) item).getStatus();

            View operateContainer = helper.getView(R.id.ll_operate_container);
            CountdownView countdownView = helper.getView(R.id.cdv_task_state);

            long taskTime = 3600000 - (TimeUtils.getNowMills() - ((PayTaskState) item).getTakeTime());

            long firstTime = ((PayTaskState) item).getFirstTime();
            long nextDay = com.juying.warenqi.utils.TimeUtils.addDay(firstTime, 1);
            String nextDayStr = TimeUtils.millis2String(nextDay, new SimpleDateFormat("MM月dd日", Locale.getDefault()));

            long secondTime = ((PayTaskState) item).getSecondTime();
            long nextDay2 = com.juying.warenqi.utils.TimeUtils.addDay(secondTime, 1);
            String nextDayStr2 = TimeUtils.millis2String(nextDay2, new SimpleDateFormat("MM月dd日", Locale.getDefault()));

            long thirdTime = ((PayTaskState) item).getThirdTime();
            long nextDay3 = com.juying.warenqi.utils.TimeUtils.addDay(thirdTime, 1);
            String nextDayStr3 = TimeUtils.millis2String(nextDay3, new SimpleDateFormat("MM月dd日", Locale.getDefault()));
            switch (status) {
                case FIRST_VISIT:
                    if (TaskTypes.FIRST_DAY_BUY.equals(sdType))
                        tagList.add(new TaskTag("待支付", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                                R.drawable.shape_stroke_yellow_2dp_radius_rect));
                    else
                        tagList.add(new TaskTag("待浏览", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                                R.drawable.shape_stroke_yellow_2dp_radius_rect));

                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.VISIBLE);
                    btnStartTask.setVisibility(View.VISIBLE);
                    helper.setText(R.id.tv_task_count_time, R.string.task_count_down_time)
                            .setText(R.id.btn_start_task, "开始任务");

                    if (((PayTaskState) item).isDelay())
                        btnCancel.setVisibility(View.GONE);
                    else {
                        if (taskTime < 15 * 60 * 1000) {
                            btnCancel.setVisibility(View.VISIBLE);
                        } else {
                            btnCancel.setVisibility(View.GONE);
                        }
                    }
                    countdownView.start(taskTime);
                    break;
                case SECOND_VISIT:
                    tagList.add(new TaskTag("待回访", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_yellow_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.VISIBLE);
                    helper.setText(R.id.btn_start_task, "回访店铺");
                    if (!TimeUtils.isToday(firstTime) && com.juying.warenqi.utils.TimeUtils.isAfterTime(10)) {
                        helper.setText(R.id.tv_task_count_time, nextDayStr + " 24:00 前完成任务");
                    } else {
                        helper.setText(R.id.tv_task_count_time, nextDayStr + " 10:00~24:00 可操作")
                                .setTextColor(R.id.tv_task_count_time, resources.getColor(R.color.grey_999))
                                .setBackgroundRes(R.id.btn_start_task, R.drawable.shape_solid_grey_2dp_rect);
                    }
                    break;
                case THIRD_VISIT:
                    tagList.add(new TaskTag("再次回访", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_yellow_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.VISIBLE);
                    helper.setText(R.id.btn_start_task, "再次回访");
                    if (!TimeUtils.isToday(secondTime) && com.juying.warenqi.utils.TimeUtils.isAfterTime(10)) {
                        helper.setText(R.id.tv_task_count_time, nextDayStr2 + " 24:00 前完成任务");
                    } else {
                        helper.setText(R.id.tv_task_count_time, nextDayStr2 + " 10:00~24:00 可操作")
                                .setTextColor(R.id.tv_task_count_time, resources.getColor(R.color.grey_999))
                                .setBackgroundRes(R.id.btn_start_task, R.drawable.shape_solid_grey_2dp_rect);
                    }
                    break;
                case WAIT_PAY:
                    tagList.add(new TaskTag("待支付", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_yellow_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.VISIBLE);
                    btnStartTask.setText("下单支付");
                    switch (sdType) {
                        case TaskTypes.ADVANCE_PAY:
                            if (((PayTaskState) item).isDelay())
                                btnCancel.setVisibility(View.GONE);
                            else {
                                if (taskTime < 15 * 60 * 1000) {
                                    btnCancel.setVisibility(View.VISIBLE);
                                } else {
                                    btnCancel.setVisibility(View.GONE);
                                }
                            }
                            countdownView.start(taskTime);
                            break;
                        case TaskTypes.SECOND_DAY_BUY:
                            if (!TimeUtils.isToday(firstTime) && com.juying.warenqi.utils.TimeUtils.isAfterTime(10)) {
                                helper.setText(R.id.tv_task_count_time, nextDayStr + " 24:00 前完成任务");
                            } else {
                                helper.setText(R.id.tv_task_count_time, nextDayStr + " 10:00~24:00 可操作")
                                        .setTextColor(R.id.tv_task_count_time, resources.getColor(R.color.grey_999))
                                        .setBackgroundRes(R.id.btn_start_task, R.drawable.shape_solid_grey_2dp_rect);
                            }
                            break;
                        case TaskTypes.THIRD_DAY_BUY:
                            if (!TimeUtils.isToday(secondTime) && com.juying.warenqi.utils.TimeUtils.isAfterTime(10)) {
                                helper.setText(R.id.tv_task_count_time, nextDayStr2 + " 24:00 前完成任务");
                            } else {
                                helper.setText(R.id.tv_task_count_time, nextDayStr2 + " 10:00~24:00 可操作")
                                        .setTextColor(R.id.tv_task_count_time, resources.getColor(R.color.grey_999))
                                        .setBackgroundRes(R.id.btn_start_task, R.drawable.shape_solid_grey_2dp_rect);
                            }
                            break;
                        case TaskTypes.FOURTH_DAY_BUY:
                            if (!TimeUtils.isToday(thirdTime) && com.juying.warenqi.utils.TimeUtils.isAfterTime(10)) {
                                helper.setText(R.id.tv_task_count_time, nextDayStr3 + " 24:00 前完成任务");
                            } else {
                                helper.setText(R.id.tv_task_count_time, nextDayStr3 + " 10:00~24:00 可操作")
                                        .setTextColor(R.id.tv_task_count_time, resources.getColor(R.color.grey_999))
                                        .setBackgroundRes(R.id.btn_start_task, R.drawable.shape_solid_grey_2dp_rect);
                            }
                            break;
                    }
                    break;
                case WAIT_SEND_GOODS:
                    tagList.add(new TaskTag("待发货", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.GONE);
                    helper.setText(R.id.tv_task_count_time, "商家正在审核任务，并将在24小时内发货");
                    break;
                case WAIT_EXAMINE:
                    tagList.add(new TaskTag(status.getTitle(), 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.GONE);
                    helper.setText(R.id.tv_task_count_time, "商家正在审核好评，通过即可完成任务");
                    break;
                case PASS:
                    tagList.add(new TaskTag("待发放", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    break;
                case FINISHED:
                    tagList.add(new TaskTag(status.getTitle(), 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.GONE);
                    countdownView.setVisibility(View.INVISIBLE);
                    helper.setText(R.id.tv_task_count_time, "恭喜您已完成此任务");
                    break;
                case WAIT_COMMENT:
                    tagList.add(new TaskTag("待好评", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.VISIBLE);
                    helper.setText(R.id.tv_task_count_time, "务必签收后再评价");
                    helper.setText(R.id.btn_start_task, "提交好评");
                    break;
                case APPEAL:
                    tagList.add(new TaskTag(status.getTitle(), 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    helper.setText(R.id.tv_task_count_time, "任务审核未通过，进入查看原因");
                    break;
                case NOPASS:
                    tagList.add(new TaskTag("申诉中", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    operateContainer.setVisibility(View.VISIBLE);
                    countdownView.setVisibility(View.INVISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    btnStartTask.setVisibility(View.GONE);
                    helper.setText(R.id.tv_task_count_time, "好评审核未通过，进入查看原因");
                    break;
                case CANCEL:
                    tagList.add(new TaskTag(status.getTitle(), 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    operateContainer.setVisibility(View.GONE);
                    break;
            }
        } else if (item instanceof AskTaskState) {
            tagList.clear();
            taskId = "任务编号：" + ((AskTaskState) item).getId();
            String goldStr;
            String noteStr;
            if (TaskTypes.ASK_TASK.equals(((AskTaskState) item).getType())) {
                timeStr = "回答截止日：" + TimeUtils.millis2String(((AskTaskState) item).getEndTime(),
                        new SimpleDateFormat("MM-dd", Locale.getDefault()));
                tagList.add(new TaskTag("问答", 10, SizeUtils.dp2px(2), R.color.white,
                        R.drawable.shape_solid_red_2dp_radius_rect));
                goldStr = BigDecimal.valueOf(((AskTaskState) item).getQuestionIngot()).movePointLeft(2).toPlainString();
                noteStr = "回答成功收入";
                helper.setText(R.id.btn_start_task, "去回答");
            } else {
                timeStr = TimeUtils.millis2String(((AskTaskState) item).getCreateTime(),
                        new SimpleDateFormat("MM-dd", Locale.getDefault()));
                tagList.add(new TaskTag("追评", 10, SizeUtils.dp2px(2), R.color.white,
                        R.drawable.shape_solid_red_2dp_radius_rect));
                goldStr = BigDecimal.valueOf(((AskTaskState) item).getAfterCommentIngot()).movePointLeft(2).toPlainString();
                noteStr = "追评成功收入";
                helper.setText(R.id.btn_start_task, "去追评");
            }

            itemPicUrl = ((AskTaskState) item).getItemPicUrl();

            spannableStringBuilder =
                    new SpanUtils()
                            .appendImage(R.drawable.ic_alibuyer, SpanUtils.ALIGN_CENTER)
                            .appendSpace(10)
                            .appendLine(((AskTaskState) item).getBuyerAccountNick())
                            .setForegroundColor(resources.getColor(R.color.grey_333))
                            .setFontSize(13, true)
                            .append(goldStr)
                            .setForegroundColor(resources.getColor(R.color.yellow_ffb64d))
                            .setBold()
                            .setFontSize(27, true)
                            .appendLine()
                            .appendLine(noteStr)
                            .setForegroundColor(resources.getColor(R.color.grey_999))
                            .setFontSize(13, true).create();

            TaskStatus status = ((AskTaskState) item).getStatus();

            View btnStart = helper.getView(R.id.btn_start_task);
            helper.getView(R.id.ll_operate_container).setVisibility(View.VISIBLE);
            helper.getView(R.id.cdv_task_state).setVisibility(View.INVISIBLE);
            switch (status) {
                case WAIT_BUYER_SUBMIT:
                    btnStart.setVisibility(View.VISIBLE);
                    helper.setTextColor(R.id.tv_task_count_time, resources.getColor(R.color.grey_666));
                    if (TaskTypes.ASK_TASK.equals(((AskTaskState) item).getType())) {
                        helper.setText(R.id.tv_task_count_time, "超过回答截止日期任务自动取消");
                    } else {
                        helper.setText(R.id.tv_task_count_time, "请在10日内完成任务");
                    }
                    break;
                case SELLER_CONFIRM:
                    tagList.add(new TaskTag("申请审核中", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    btnStart.setVisibility(View.GONE);
                    helper.setText(R.id.tv_task_count_time, "商家24小时内审核，通过将派发问答任务");
                    break;
                case WAIT_EXAMINE:
                    tagList.add(new TaskTag("待审核", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    btnStart.setVisibility(View.GONE);
                    if (TaskTypes.ASK_TASK.equals(((AskTaskState) item).getType())) {
                        helper.setText(R.id.tv_task_count_time, "商家正在审核回答，通过后即可完成任务");
                    } else {
                        helper.setText(R.id.tv_task_count_time, "商家正在审核追评，通过后即可完成任务");
                    }
                    break;
                case APPEAL:
                    tagList.add(new TaskTag("申诉中", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    btnStart.setVisibility(View.GONE);
                    if (TaskTypes.ASK_TASK.equals(((AskTaskState) item).getType())) {
                        helper.setText(R.id.tv_task_count_time, "回答任务审核未通过，进入查看原因");
                    } else {
                        helper.setText(R.id.tv_task_count_time, "追评任务审核未通过，进入查看原因");
                    }
                    break;
                case FINISHED:
                    tagList.add(new TaskTag("已完成", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    btnStart.setVisibility(View.GONE);
                    if (TaskTypes.ASK_TASK.equals(((AskTaskState) item).getType())) {
                        helper.setText(R.id.tv_task_count_time, "恭喜您已完成该问答任务");
                    } else {
                        helper.setText(R.id.tv_task_count_time, "恭喜您已完成该追评任务");
                    }
                    break;
                case CANCEL:
                    tagList.add(new TaskTag("已取消", 10, SizeUtils.dp2px(2), R.color.blue_4680fe,
                            R.drawable.shape_stroke_blue_2dp_radius_rect));
                    btnStart.setVisibility(View.GONE);
                    helper.setText(R.id.tv_task_count_time, ((AskTaskState) item).getMemo());
                    break;
            }
        }

        Glide.with(mContext.getApplicationContext())
                .load(itemPicUrl)
                .centerCrop()
                .into((ImageView) helper.getView(R.id.iv_item_img));

        helper.setText(R.id.tv_task_id, taskId)
                .setText(R.id.tv_task_time, timeStr)
                .setText(R.id.tv_task_info, spannableStringBuilder);

        RecyclerView recyclerView = helper.getView(R.id.rv_task_state_tags);
        if (tagList.size() > 0) {
            TaskTagAdapter tagAdapter = new TaskTagAdapter(tagList);
            recyclerView.setAdapter(tagAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                    LinearLayoutManager.HORIZONTAL, false));
            recyclerView.removeItemDecoration(itemDecoration);
            recyclerView.addItemDecoration(itemDecoration);
        }
    }

    private RecyclerView.ItemDecoration itemDecoration = new RecyclerView.ItemDecoration() {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(10, 0, 0, 0);
        }
    };
}
