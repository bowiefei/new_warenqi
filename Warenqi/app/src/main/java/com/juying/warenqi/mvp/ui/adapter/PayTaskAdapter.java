package com.juying.warenqi.mvp.ui.adapter;

import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juying.warenqi.R;
import com.juying.warenqi.enums.TaskTypes;
import com.juying.warenqi.mvp.model.entity.PayTaskList;
import com.juying.warenqi.mvp.model.entity.TaskTag;
import com.juying.warenqi.mvp.ui.holder.BaseAutoLayoutHolder;
import com.juying.warenqi.view.RoundedBackgroundSpan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/1 16:49
 * </pre>
 */
public class PayTaskAdapter extends BaseQuickAdapter<PayTaskList, BaseAutoLayoutHolder> {

    private final boolean isOldVersion;

    public PayTaskAdapter(@Nullable List<PayTaskList> data) {
        super(R.layout.item_pay_task, data);
        isOldVersion = SPUtils.getInstance().getBoolean("isOldVersion");
    }

    @Override
    protected void convert(BaseAutoLayoutHolder helper, PayTaskList item) {
        Resources resources = mContext.getApplicationContext().getResources();

        String sdType = item.getSdType();
        if (!TaskTypes.ADVANCE_PAY.equals(sdType)) {
            String str = "";
            int drawableRes = 0;
            switch (sdType) {
                case "FIRST_DAY_BUY":
                    str = "当天浏览收藏商品并下单";
                    drawableRes = R.drawable.pic_day1;
                    break;
                case "SECOND_DAY_BUY":
                    str = "任务第2天下单购买商品";
                    drawableRes = R.drawable.pic_day2;
                    break;
                case "THIRD_DAY_BUY":
                    str = "任务第3天下单购买商品";
                    drawableRes = R.drawable.pic_day3;
                    break;
                case "FOURTH_DAY_BUY":
                    str = "任务第4天下单购买商品";
                    drawableRes = R.drawable.pic_day4;
                    break;
            }
            TextView textView = helper.getView(R.id.tv_days_type);
            textView.setText(str);
            Drawable lineDrawable = resources.getDrawable(R.drawable.shape_rv_divider);
            lineDrawable.setBounds(0, 0, ScreenUtils.getScreenWidth(),
                    lineDrawable.getIntrinsicHeight());
            Drawable iconDrawable = resources.getDrawable(drawableRes);
            iconDrawable.setBounds(0, 0, iconDrawable.getIntrinsicWidth(),
                    iconDrawable.getIntrinsicHeight());
            textView.setCompoundDrawables(null, null, iconDrawable,
                    lineDrawable);
        }

        SpanUtils spanUtils = new SpanUtils()
                .appendLine(item.getSellerNick())
                .appendLine()
                .append(BigDecimal.valueOf(item.getIngot()).movePointLeft(2).toPlainString())
                .setForegroundColor(resources.getColor(R.color.yellow_ffb64d))
                .setFontSize(27, true);
        int extraReward = item.getExtraReward();
        if (extraReward > 0)
            spanUtils.append("+")
                    .setFontSize(25, true)
                    .setForegroundColor(resources.getColor(R.color.grey_dedede))
                    .append(BigDecimal.valueOf(extraReward).movePointLeft(2).toPlainString())
                    .setForegroundColor(resources.getColor(R.color.yellow_ffb64d))
                    .setFontSize(27, true)
                    .appendSpace(SizeUtils.dp2px(10))
                    .append("加赏")
                    .setSpans(new RoundedBackgroundSpan(resources.getColor(R.color.red_fb607f),
                            resources.getColor(R.color.white), SizeUtils.sp2px(10)
                            , SizeUtils.dp2px(2)));
        SpannableStringBuilder stringBuilder = spanUtils
                .appendLine()
                .appendLine()
                .append("预计游戏收入")
                .setForegroundColor(resources.getColor(R.color.grey_666))
                .setFontSize(12, true).create();
        helper.setText(R.id.tv_pay_task_info, stringBuilder)
                .setText(R.id.tv_task_price, "预付金额：" + item.getItemPriceStr())
                .setText(R.id.btn_take_task, isOldVersion ? "不能领取" : "马上领取")
                .setVisible(R.id.tv_days_type, !TaskTypes.ADVANCE_PAY.equals(sdType))
                .setTextColor(R.id.btn_take_task, isOldVersion ?
                        resources.getColor(R.color.grey_a0a0a0) : resources.getColor(R.color.white));

        View view = helper.getView(R.id.btn_take_task);
        view.setBackgroundResource(isOldVersion ? R.drawable.shape_stroke_green_4dp_radius_btn :
                R.drawable.shape_blue_2dp_radius_rect);

        RecyclerView recyclerView = helper.getView(R.id.rv_pay_task_tags);

        List<TaskTag> tagList = new ArrayList<>();
        tagList.clear();
        if (item.isTaojinbi())
            tagList.add(new TaskTag("信用卡", R.color.white, R.drawable.shape_solid_yellow_2dp_radius_rect));
        if (item.isHuabei())
            tagList.add(new TaskTag("花呗", R.color.white, R.drawable.shape_solid_yellow_2dp_radius_rect));
        if (!TextUtils.isEmpty(item.getMessage()))
            tagList.add(new TaskTag("留言", R.color.yellow_ffb64d, R.drawable.shape_stroke_yellow_2dp_radius_rect));
        if (item.isChat())
            tagList.add(new TaskTag("聊天", R.color.yellow_ffb64d, R.drawable.shape_stroke_yellow_2dp_radius_rect));
        if (tagList.size() > 0) {
            TaskTagAdapter tagAdapter = new TaskTagAdapter(tagList);
            recyclerView.setAdapter(tagAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                    LinearLayoutManager.HORIZONTAL, false));
            recyclerView.removeItemDecoration(itemDecoration);
            recyclerView.addItemDecoration(itemDecoration);
        }
    }

    private RecyclerView.ItemDecoration itemDecoration = new RecyclerView.ItemDecoration() {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(10, 0, 0, 0);
        }
    };
}
