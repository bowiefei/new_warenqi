package com.juying.warenqi.mvp.model.api.service;

import com.juying.warenqi.mvp.model.entity.AskTaskState;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.FlowTaskList;
import com.juying.warenqi.mvp.model.entity.PayTaskList;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/6/29 21:39
 * </pre>
 */
public interface TaskListService {

    @GET("user/app/listFlowTask")
    Observable<BaseBean<List<FlowTaskList>>> getFlowTaskList(@Query("vo.pageNo") int pageNo,
                                                             @Query("vo.pageSize") int pageSize,
                                                             @Query("vo.taskType") String taskType,
                                                             @Query("buyerAccountId") long buyerAccountId);

    @GET("app/sdsub/listSDTask")
    Observable<BaseBean<List<PayTaskList>>> getPayTaskList(@Query("vo.pageNo") int pageNo,
                                                           @Query("vo.pageSize") int pageSize,
                                                           @Query("vo.sdType") String taskType,
                                                           @Query("buyerAccountId") long buyerAccountId);

    @GET("app/buyer/listApplayQuestionTask")
    Observable<BaseBean<List<AskTaskState>>> getAskTaskList(@Query("vo.pageNo") int pageNo,
                                                            @Query("vo.pageSize") int pageSize);
}
