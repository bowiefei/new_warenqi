package com.juying.warenqi.mvp.model;

import android.app.Application;

import com.google.gson.Gson;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;
import com.juying.warenqi.app.Constants;
import com.juying.warenqi.mvp.contract.TaskListContract;
import com.juying.warenqi.mvp.model.api.cache.CommonCache;
import com.juying.warenqi.mvp.model.api.service.CommonService;
import com.juying.warenqi.mvp.model.api.service.TaskListService;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.FlowTaskList;
import com.juying.warenqi.mvp.model.entity.PayTaskList;
import com.juying.warenqi.mvp.model.entity.TaobaoAccount;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.rx_cache2.DynamicKey;
import io.rx_cache2.EvictDynamicKey;
import io.rx_cache2.Reply;


@ActivityScope
public class TaskListModel extends BaseModel implements TaskListContract.Model {
    private Gson mGson;
    private Application mApplication;

    @Inject
    public TaskListModel(IRepositoryManager repositoryManager, Gson gson, Application application) {
        super(repositoryManager);
        this.mGson = gson;
        this.mApplication = application;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mGson = null;
        this.mApplication = null;
    }

    @Override
    public Observable<List<TaobaoAccount>> getAccountList() {
        return mRepositoryManager.obtainRetrofitService(CommonService.class)
                .getAccountList("TAOBAO", true)
                .flatMap(taobaoAccountBaseBean ->
                        Observable.just(taobaoAccountBaseBean.getResults()));
    }

    @Override
    public Observable<List<FlowTaskList>> getFlowTaskList(int pageNo, String taskType,
                                                          long buyerAccountId, long idLastQueried,
                                                          boolean isUpdate) {
        Observable<List<FlowTaskList>> flowTaskList =
                mRepositoryManager.obtainRetrofitService(TaskListService.class)
                        .getFlowTaskList(pageNo, Constants.LIST_PAGE_SIZE, taskType, buyerAccountId)
                        .map(BaseBean::getResults);
        return mRepositoryManager.obtainCacheService(CommonCache.class)
                .getFlowTaskList(flowTaskList, new DynamicKey(idLastQueried)
                        , new EvictDynamicKey(isUpdate))
                .flatMap(new Function<Reply<List<FlowTaskList>>, ObservableSource<List<FlowTaskList>>>() {
                    @Override
                    public ObservableSource<List<FlowTaskList>> apply(@NonNull Reply<List<FlowTaskList>> listReply) throws Exception {
                        return Observable.just(listReply.getData());
                    }
                });
    }

    @Override
    public Observable<BaseBean<List<PayTaskList>>> getPayTaskList(int pageNo, String taskType,
                                                                  long buyerAccountId, boolean isUpdate) {
        return mRepositoryManager.obtainRetrofitService(TaskListService.class)
                .getPayTaskList(pageNo, Constants.LIST_PAGE_SIZE, taskType, buyerAccountId);
    }
}