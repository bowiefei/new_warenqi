package com.juying.warenqi.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/30 13:54
 * </pre>
 */
@StringDef({TaskTypes.NORMAL, TaskTypes.DIRECT, TaskTypes.ACTIVITY,
        TaskTypes.ADVANCE_PAY, TaskTypes.SEVERAL_DAYS})
@Retention(RetentionPolicy.SOURCE)
public @interface TaskTypes {
    String NORMAL = "ORDER";
    String DIRECT = "SUBWAY";
    String ACTIVITY = "ZWTG";
    String ADVANCE_PAY = "DIANFU";
    String SEVERAL_DAYS = "DAYS_DIANFU";
    String FIRST_DAY_BUY = "FIRST_DAY_BUY";
    String SECOND_DAY_BUY = "SECOND_DAY_BUY";
    String THIRD_DAY_BUY = "THIRD_DAY_BUY";
    String FOURTH_DAY_BUY = "FOURTH_DAY_BUY";
    String ASK_TASK = "QUESTION";
}
