package com.juying.warenqi.mvp.model.entity;

import com.juying.warenqi.enums.TaskStatus;
import com.juying.warenqi.enums.TaskTypeEnum;

import java.util.List;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/2 14:00
 * </pre>
 */
public class FlowTaskState {
    private long buyerAccountId;
    private String buyerAccountNick;
    private long buyerId;
    private String buyerNick;
    private boolean chatNum;
    private boolean collectProduct;
    private boolean collectShop;
    private boolean commectNum;
    private boolean couponNum;
    private long createTime;
    private long id;
    private long ingot;
    private String kwd;
    private boolean otherNum;
    private int pageNo;
    private int pageSize;
    private int price;
    private int reward;
    private long sellerId;
    private String sellerNick;
    private String shopName;
    private boolean shoppingCart;
    private int startIndex;
    private TaskStatus status;
    private boolean tags;
    private long takenTime;
    private long taskId;
    private String taskIdStr;
    private String taskImage;
    private String taskRequest;
    private TaskTypeEnum taskType;
    private int totalCount;
    private int totalPageCount;
    private long updateTime;
    private int userIngotSum;
    private List<?> items;

    public long getBuyerAccountId() {
        return buyerAccountId;
    }

    public void setBuyerAccountId(long buyerAccountId) {
        this.buyerAccountId = buyerAccountId;
    }

    public String getBuyerAccountNick() {
        return buyerAccountNick;
    }

    public void setBuyerAccountNick(String buyerAccountNick) {
        this.buyerAccountNick = buyerAccountNick;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerNick() {
        return buyerNick;
    }

    public void setBuyerNick(String buyerNick) {
        this.buyerNick = buyerNick;
    }

    public boolean isChatNum() {
        return chatNum;
    }

    public void setChatNum(boolean chatNum) {
        this.chatNum = chatNum;
    }

    public boolean isCollectProduct() {
        return collectProduct;
    }

    public void setCollectProduct(boolean collectProduct) {
        this.collectProduct = collectProduct;
    }

    public boolean isCollectShop() {
        return collectShop;
    }

    public void setCollectShop(boolean collectShop) {
        this.collectShop = collectShop;
    }

    public boolean isCommectNum() {
        return commectNum;
    }

    public void setCommectNum(boolean commectNum) {
        this.commectNum = commectNum;
    }

    public boolean isCouponNum() {
        return couponNum;
    }

    public void setCouponNum(boolean couponNum) {
        this.couponNum = couponNum;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIngot() {
        return ingot;
    }

    public void setIngot(long ingot) {
        this.ingot = ingot;
    }

    public String getKwd() {
        return kwd;
    }

    public void setKwd(String kwd) {
        this.kwd = kwd;
    }

    public boolean isOtherNum() {
        return otherNum;
    }

    public void setOtherNum(boolean otherNum) {
        this.otherNum = otherNum;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerNick() {
        return sellerNick;
    }

    public void setSellerNick(String sellerNick) {
        this.sellerNick = sellerNick;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public boolean isShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(boolean shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public boolean isTags() {
        return tags;
    }

    public void setTags(boolean tags) {
        this.tags = tags;
    }

    public long getTakenTime() {
        return takenTime;
    }

    public void setTakenTime(long takenTime) {
        this.takenTime = takenTime;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getTaskIdStr() {
        return taskIdStr;
    }

    public void setTaskIdStr(String taskIdStr) {
        this.taskIdStr = taskIdStr;
    }

    public String getTaskImage() {
        return taskImage;
    }

    public void setTaskImage(String taskImage) {
        this.taskImage = taskImage;
    }

    public String getTaskRequest() {
        return taskRequest;
    }

    public void setTaskRequest(String taskRequest) {
        this.taskRequest = taskRequest;
    }

    public TaskTypeEnum getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskTypeEnum taskType) {
        this.taskType = taskType;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getUserIngotSum() {
        return userIngotSum;
    }

    public void setUserIngotSum(int userIngotSum) {
        this.userIngotSum = userIngotSum;
    }

    public List<?> getItems() {
        return items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }
}
