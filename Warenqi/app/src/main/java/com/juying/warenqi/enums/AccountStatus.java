package com.juying.warenqi.enums;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/5 15:10
 * </pre>
 */
public enum AccountStatus {
    EDITING("编辑中"),
    WAIT_EXAMINE("待审核"),
    EXAMINING("审核中"),
    EXAMINED("审核通过"),
    NOT_PASS("审核未通过"),
    LOCKED("封号中");

    private final String title;

    AccountStatus(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
