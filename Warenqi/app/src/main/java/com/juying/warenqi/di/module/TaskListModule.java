package com.juying.warenqi.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.juying.warenqi.mvp.contract.TaskListContract;
import com.juying.warenqi.mvp.model.TaskListModel;


@Module
public class TaskListModule {
    private TaskListContract.View view;

    /**
     * 构建TaskListModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public TaskListModule(TaskListContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    TaskListContract.View provideTaskListView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    TaskListContract.Model provideTaskListModel(TaskListModel model) {
        return model;
    }
}