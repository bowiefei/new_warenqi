package com.juying.warenqi.mvp.ui.adapter;

import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.model.entity.FlowTaskList;
import com.juying.warenqi.mvp.model.entity.TaskTag;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/30 13:26
 * </pre>
 */
public class FlowTaskAdapter extends BaseQuickAdapter<FlowTaskList, BaseViewHolder> {

    private final boolean isOldVersion;
    private String mNoLimitStr = "不限";

    public FlowTaskAdapter(@Nullable List<FlowTaskList> data) {
        super(R.layout.item_flow_task, data);
        isOldVersion = SPUtils.getInstance().getBoolean("isOldVersion");
    }

    @Override
    protected void convert(BaseViewHolder helper, FlowTaskList item) {
        StringBuilder builder = new StringBuilder();
        builder.append("浏览+");
        if (item.isCommectNum()) builder.append("浏览评论+");
        if (item.isChatNum()) builder.append("聊天+");
        if (item.isCollectProduct()) builder.append("收藏宝贝+");
        if (item.isCollectShop()) builder.append("收藏店铺+");
        if (item.isCouponNum()) builder.append("领取优惠券+");
        if (item.isShoppingCart()) builder.append("加入购物车+");
        if (item.isOtherNum()) builder.append("做其他任务+");
        builder.deleteCharAt(builder.lastIndexOf("+"));
        SpannableStringBuilder spannableStringBuilder = new SpanUtils()
                .appendLine(item.getSellerNick()).setFontSize(15, true)
                .appendLine()
                .appendLine(builder.toString()).setFontSize(18, true)
                .create();

        String reward = BigDecimal.valueOf(item.getIngot()).movePointLeft(2).toString();
        helper.setText(R.id.tv_flow_task_info, spannableStringBuilder)
                .setText(R.id.tv_flow_task_gold, reward + "金币")
                .setText(R.id.btn_get_task, isOldVersion ? "不能领取" : "马上领取")
                .addOnClickListener(R.id.btn_get_task);

        RecyclerView recyclerView = helper.getView(R.id.rv_flow_task_tags);
        List<TaskTag> tagList = new ArrayList<>();
        tagList.clear();
        String genderLimit = item.getGenderLimit();
        if (!TextUtils.isEmpty(genderLimit) && !mNoLimitStr.equals(genderLimit))
            tagList.add(new TaskTag(genderLimit, R.color.yellow_ffb64d, R.drawable.shape_stroke_yellow_2dp_radius_rect));
        String areaLimit = item.getAreaLimit();
        if (!TextUtils.isEmpty(areaLimit) && !mNoLimitStr.equals(areaLimit))
            tagList.add(new TaskTag(areaLimit, R.color.yellow_ffb64d, R.drawable.shape_stroke_yellow_2dp_radius_rect));
        String creditLimit = item.getCreditLimit();
        if (!TextUtils.isEmpty(creditLimit) && !mNoLimitStr.equals(creditLimit))
            tagList.add(new TaskTag(creditLimit, R.color.yellow_ffb64d, R.drawable.shape_stroke_yellow_2dp_radius_rect));
        String ageLimit = item.getAgeLimit();
        if (!TextUtils.isEmpty(ageLimit) && !mNoLimitStr.equals(ageLimit))
            tagList.add(new TaskTag(ageLimit, R.color.yellow_ffb64d, R.drawable.shape_stroke_yellow_2dp_radius_rect));
        String commdityType = item.getCommdityType();
        if (!TextUtils.isEmpty(commdityType) && !mNoLimitStr.equals(commdityType))
            tagList.add(new TaskTag(commdityType, R.color.yellow_ffb64d, R.drawable.shape_stroke_yellow_2dp_radius_rect));
        if (tagList.size() > 0) {
            TaskTagAdapter tagAdapter = new TaskTagAdapter(tagList);
            recyclerView.setAdapter(tagAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                    LinearLayoutManager.HORIZONTAL, false));
            recyclerView.removeItemDecoration(itemDecoration);
            recyclerView.addItemDecoration(itemDecoration);
        }
    }

   private RecyclerView.ItemDecoration itemDecoration = new RecyclerView.ItemDecoration() {
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            if (parent.getChildLayoutPosition(view) != 0)
                outRect.set(10, 0, 0, 0);
        }
    };
}
