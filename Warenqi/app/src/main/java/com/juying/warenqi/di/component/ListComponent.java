package com.juying.warenqi.di.component;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Component;

import com.jess.arms.di.component.AppComponent;

import com.juying.warenqi.di.module.ListModule;

import com.juying.warenqi.mvp.ui.activity.ListActivity;

@ActivityScope
@Component(modules = ListModule.class, dependencies = AppComponent.class)
public interface ListComponent {
    void inject(ListActivity activity);
}