package com.juying.warenqi.di.component;

import com.jess.arms.di.component.AppComponent;
import com.jess.arms.di.scope.ActivityScope;
import com.juying.warenqi.di.module.TaskStateListModule;
import com.juying.warenqi.mvp.ui.activity.TaskStateListActivity;

import dagger.Component;

@ActivityScope
@Component(modules = TaskStateListModule.class, dependencies = AppComponent.class)
public interface TaskStateListComponent {
    void inject(TaskStateListActivity activity);
}