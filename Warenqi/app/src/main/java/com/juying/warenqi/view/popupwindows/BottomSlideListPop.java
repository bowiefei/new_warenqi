package com.juying.warenqi.view.popupwindows;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jess.arms.utils.UiUtils;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.ui.adapter.PopListAdapter;
import com.juying.warenqi.view.DividerLinearItemDecoration;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/29 11:31
 * </pre>
 */
public class BottomSlideListPop extends BasePopupWindow {
    private View mPopView;
    private RecyclerView mRecyclerView;
    private final PopListAdapter mAdapter;

    public BottomSlideListPop(Activity context, List<PopTitle> list) {
        super(context);
        mAdapter = new PopListAdapter(list);
        mRecyclerView.setAdapter(mAdapter);
        UiUtils.configRecycleView(mRecyclerView, new LinearLayoutManager(context));
        mRecyclerView.addItemDecoration(new DividerLinearItemDecoration(context, DividerLinearItemDecoration.VERTICAL_LIST));
    }

    public void setPopListClickListener(BaseQuickAdapter.OnItemClickListener listener) {
        mAdapter.setOnItemClickListener(listener);
    }

    @Override
    protected Animation initShowAnimation() {
        return getTranslateAnimation(250 * 2, 0, 200);
    }

    @Override
    public View getClickToDismissView() {
        return mPopView;
    }

    @Override
    public View onCreatePopupView() {
        mPopView = createPopupById(R.layout.popup_bottom_slide_list);
        mRecyclerView = (RecyclerView) mPopView.findViewById(R.id.rv_pop_list);
        return mPopView;
    }

    @Override
    public View initAnimaView() {
        return mRecyclerView;
    }
}
