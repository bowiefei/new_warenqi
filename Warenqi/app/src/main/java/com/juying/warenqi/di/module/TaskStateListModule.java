package com.juying.warenqi.di.module;

import com.jess.arms.di.scope.ActivityScope;
import com.juying.warenqi.mvp.contract.TaskStateListContract;
import com.juying.warenqi.mvp.model.TaskStateListModel;

import dagger.Module;
import dagger.Provides;


@Module
public class TaskStateListModule {
    private TaskStateListContract.View view;

    /**
     * 构建ListModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public TaskStateListModule(TaskStateListContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    TaskStateListContract.View provideListView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    TaskStateListContract.Model provideListModel(TaskStateListModel model) {
        return model;
    }
}