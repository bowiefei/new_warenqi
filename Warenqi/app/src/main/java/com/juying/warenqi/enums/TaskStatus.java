package com.juying.warenqi.enums;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/2 16:54
 * </pre>
 */
public enum TaskStatus {
    WAIT("待操作"),
    SELLER_PROCESS("商家处理中"),
    APPEAL("申诉中"),
    FINISHED("已完成"),
    WAIT_COMMENT("待好评"),
    CANCEL("已取消"),
    WAIT_PUBLISH("待发布"),
    PUBLISHED("已发布"),
    WAIT_EXAMINE("待审核"),
    TAKEN("已接手"),
    NOPASS("审核不通过"),
    ALL("全部"),
    PASS("审核通过"),
    WAIT_PAY("待支付"),
    WAIT_SEND_GOODS("待发货"),
    FIRST_VISIT("待收藏"),
    SECOND_VISIT("待第二次访问"),
    THIRD_VISIT("待第三次访问"),
    WAIT_WIDTHDRAW("待提现"),
    SELLER_CONFIRM("待确认邀请"),

    WAIT_BUYER_SUBMIT("待买手提交任务"), WAIT_SELLER("待商家处理");
    private final String title;

    TaskStatus(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public static String getTaskStatus(String title) {
        for (TaskStatus taskStatus : TaskStatus.values()) {
            if (title.equals(taskStatus.getTitle())) {
                return taskStatus.name();
            }
        }
        return null;
    }
}
