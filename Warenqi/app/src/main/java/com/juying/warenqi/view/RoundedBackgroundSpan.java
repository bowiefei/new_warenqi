package com.juying.warenqi.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.ColorInt;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.style.ReplacementSpan;

/**
 * <pre>
 * Author: @LvYan
 * Description:给SpannableString加圆角背景
 * Date: 2017/7/1 21:08
 * </pre>
 */
public class RoundedBackgroundSpan extends ReplacementSpan {

    @ColorInt
    private int mBgColor;
    @ColorInt
    private int mTextColor;
    private int mTextSize;
    private int mRadius;

    public RoundedBackgroundSpan(int bgColor, int textColor, int textSize, int radius) {
        this.mBgColor = bgColor;
        this.mTextColor = textColor;
        this.mTextSize = textSize;
        this.mRadius = radius;
    }

    @Override
    public int getSize(@NonNull Paint paint, CharSequence charSequence, @IntRange(from = 0) int start, @IntRange(from = 0) int end, @Nullable Paint.FontMetricsInt fontMetricsInt) {
        return Math.round(paint.measureText(charSequence, start, end));
    }

    @Override
    public void draw(@NonNull Canvas canvas, CharSequence text, @IntRange(from = 0) int start, @IntRange(from = 0) int end, float x, int top, int y, int bottom, @NonNull Paint paint) {
        paint.setTextSize(mTextSize);
        RectF rect = new RectF(x - mRadius, bottom - mTextSize - mRadius * 5, x + paint.measureText(text, start, end) + mRadius, y + mRadius);
        paint.setColor(mBgColor);
        canvas.drawRoundRect(rect, mRadius, mRadius, paint);
        paint.setColor(mTextColor);
        canvas.drawText(text, start, end, x, y - mRadius, paint);
    }
}
