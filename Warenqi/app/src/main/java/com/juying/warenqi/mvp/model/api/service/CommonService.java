package com.juying.warenqi.mvp.model.api.service;


import com.juying.warenqi.mvp.model.entity.AccountInfo;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.TaobaoAccount;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * 存放通用的一些API
 */
public interface CommonService {

    @GET("app/index")
    Observable<BaseBean<AccountInfo>> getAccountInfo();

    @GET("buyer/account/list")
    Observable<BaseBean<List<TaobaoAccount>>> getAccountList(@Query("platform") String platform,
                                                             @Query("receive") boolean receive);

    @GET("user/upload/token")
    Observable<BaseBean<String>> getQiniuToken();

}
