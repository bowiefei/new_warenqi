package com.juying.warenqi.mvp.model.api.service;

import com.juying.warenqi.mvp.model.entity.AccountAuditInfo;
import com.juying.warenqi.mvp.model.entity.BaseBean;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/5 10:16
 * </pre>
 */
public interface ListService {

    @GET("app/logout")
    Observable<BaseBean> logout();

    @GET("user/account/fundAccountInfo")
    Observable<BaseBean<AccountAuditInfo>> getAccountAuditInfo();

    @FormUrlEncoded
    @POST("user/account/saveAccoundInfo")
    Observable<BaseBean> submitAuditInfo(@Field("vo.name") String name,
                                         @Field("vo.idcard") String idcard,
                                         @Field("vo.idcardImg") String idcardImg,
                                         @Field("vo.handImg") String handImg);

}
