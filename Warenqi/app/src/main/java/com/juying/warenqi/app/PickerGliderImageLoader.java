package com.juying.warenqi.app;

import android.app.Activity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.juying.warenqi.R;
import com.lzy.imagepicker.loader.ImageLoader;


/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/6 13:31
 * </pre>
 */
public class PickerGliderImageLoader implements ImageLoader {
    @Override
    public void displayImage(Activity activity, String path, ImageView imageView, int width, int height) {
        Glide.with(activity)
                .load(path)
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(imageView);
    }

    @Override
    public void clearMemoryCache() {

    }
}
