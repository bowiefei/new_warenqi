package com.juying.warenqi.view.popupwindows;

import android.support.annotation.ColorInt;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/29 11:54
 * </pre>
 */
public class PopTitle {
    private String title;
    @ColorInt
    private int textColor;
    private int textSize;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }
}
