package com.juying.warenqi.mvp.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.CleanUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.UiUtils;
import com.juying.warenqi.R;
import com.juying.warenqi.app.Constants;
import com.juying.warenqi.di.component.DaggerListComponent;
import com.juying.warenqi.di.module.ListModule;
import com.juying.warenqi.mvp.contract.ListContract;
import com.juying.warenqi.mvp.model.entity.AccountInfoSectionContent;
import com.juying.warenqi.mvp.presenter.ListPresenter;
import com.juying.warenqi.mvp.ui.adapter.MultiItemAdapter;
import com.juying.warenqi.view.DividerLinearItemDecoration;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.weavey.loading.lib.LoadingLayout;

import java.util.List;

import butterknife.BindView;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class ListActivity extends BaseActivity<ListPresenter> implements ListContract.View {


    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.loading_layout)
    LoadingLayout loadingLayout;
    @BindView(R.id.refresh_layout)
    TwinklingRefreshLayout refreshLayout;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_right_iv)
    ImageView toolbarRightIv;
    @BindView(R.id.toolbar_right_tv)
    TextView toolbarRightTv;
    private AlertDialog mClearCacheDialog;
    private AlertDialog mLogoutDialog;
    private int mClickedIvId;
    private ProgressDialog mProgressDialog;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerListComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .listModule(new ListModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_list; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        String type = intent.getStringExtra("type");
        toolbarTitle.setText(type);
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadmore(false);

        loadingLayout.setOnReloadListener(v -> initRequest(type));

        initRequest(type);

        mClearCacheDialog = new AlertDialog.Builder(ListActivity.this)
                .setTitle("提示")
                .setMessage("确定要清除缓存吗？")
                .setNegativeButton("取消", null)
                .setPositiveButton("确定", (dialog, which) -> {
                    cleanAppCache();
                    mPresenter.getSettingInfo();
                }).create();

        mLogoutDialog = new AlertDialog.Builder(ListActivity.this)
                .setTitle("提示")
                .setMessage("确定退出吗？")
                .setNegativeButton("取消", null)
                .setPositiveButton("确定", (dialog, which) -> {
                    cleanAppCache();
                    mPresenter.logout();
                }).create();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage("上传图片，请稍后...");
    }

    public void initRequest(String type) {
        switch (type) {
            case "设置":
                rv.addItemDecoration(new DividerLinearItemDecoration(getApplicationContext(),
                        DividerLinearItemDecoration.VERTICAL_LIST, this.getResources()
                        .getDrawable(R.drawable.shape_solid_grey_rect)));
                mPresenter.getSettingInfo();
                break;
            case "个人资料":
                rv.addItemDecoration(new DividerLinearItemDecoration(getApplicationContext(),
                        DividerLinearItemDecoration.VERTICAL_LIST, SizeUtils.dp2px(15), 0, 0, 0));
                mPresenter.getPersonalProfile();
                break;
            case "淘宝账号":
                toolbarRightIv.setVisibility(View.VISIBLE);
                toolbarRightIv.setImageResource(R.drawable.ic_add_black_36dp);
                toolbarTitle.setText("我的账号");
            case "花呗号认证":
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) rv.getLayoutParams();
                layoutParams.setMargins(0, SizeUtils.dp2px(20), 0, 0);
                rv.addItemDecoration(new DividerLinearItemDecoration(getApplicationContext(),
                        DividerLinearItemDecoration.VERTICAL_LIST, SizeUtils.dp2px(15), 0, 0, 0));
                mPresenter.getAccountList(type);
                break;
            case "实名认证":
                toolbarRightIv.setVisibility(View.VISIBLE);
                toolbarRightIv.setImageResource(R.drawable.certificate_tutorial);
                rv.addItemDecoration(new DividerLinearItemDecoration(getApplicationContext(),
                        DividerLinearItemDecoration.VERTICAL_LIST));
                mPresenter.getAccountAuditInfo();
                break;
        }
    }

    public void cleanAppCache() {
        CleanUtils.cleanInternalCache();
        CleanUtils.cleanExternalCache();
        CleanUtils.cleanInternalDbs();
        CleanUtils.cleanInternalFiles();
    }


    @Override
    public void showLoading() {
        loadingLayout.setStatus(LoadingLayout.Loading);
    }

    @Override
    public void hideLoading() {
        loadingLayout.setStatus(LoadingLayout.Success);
        refreshLayout.finishRefreshing();
        refreshLayout.finishLoadmore();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        UiUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        UiUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @Override
    public void showError() {
        loadingLayout.setStatus(LoadingLayout.Error);
        refreshLayout.finishRefreshing();
        refreshLayout.finishLoadmore();
    }

    @Override
    public void setAdapter(BaseQuickAdapter adapter) {
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));

        if (adapter instanceof MultiItemAdapter) {
            adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                @Override
                public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                    mClickedIvId = view.getId();
                    Intent intent = new Intent(ListActivity.this, ImageGridActivity.class);
                    startActivityForResult(intent, Constants.IMAGE_PICK);
                }
            });
        }

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Object item = adapter.getItem(position);
                if (item instanceof AccountInfoSectionContent) {
                    String title = ((AccountInfoSectionContent) item).getTitle();
                    if (title.contains("清除缓存"))
                        title = "清除缓存";
                    switch (title) {
                        case "版本信息":
                            ToastUtils.showShort(title);
                            break;
                        case "清除缓存":
                            mClearCacheDialog.show();
                            break;
                    }
                }
            }
        });
    }

    @Override
    public void addFooterButton(BaseQuickAdapter adapter, String text) {
        adapter.removeAllFooterView();
        Button button = new Button(this);
        button.setText(text);
        button.setTextColor(this.getResources().getColor(R.color.white));
        button.setTextSize(18);
        button.setBackgroundResource(R.drawable.shape_blue_4dp_radius_btn);
        LinearLayout.LayoutParams layoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(45));
        int dp_20 = SizeUtils.dp2px(20);
        layoutParams.setMargins(dp_20, dp_20, dp_20, dp_20);
        button.setLayoutParams(layoutParams);
        adapter.setFooterView(button);
        button.setTag(button.getText());
        button.setOnClickListener(mFooterOnClickListener);
    }

    @Override
    public void showUploadDialog() {
        mProgressDialog.show();
    }

    @Override
    public void dismissUploadDialog() {
        mProgressDialog.dismiss();
    }

    private View.OnClickListener mFooterOnClickListener = v -> {
        switch (((String) v.getTag())) {
            case "退出登录":
                mLogoutDialog.show();
                break;
            case "提交审核":
                mPresenter.submitAuditInfo();
                break;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            if (data != null && requestCode == Constants.IMAGE_PICK) {
                List<ImageItem> images = (List<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                mPresenter.uploadAuditImg(images.get(0).path, mClickedIvId);
            } else {
                Toast.makeText(this, "没有数据", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mClearCacheDialog = null;
        mLogoutDialog = null;
        mProgressDialog = null;
    }
}
