package com.juying.warenqi.app;

import android.os.Environment;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/6/18 22:21
 * </pre>
 */
public interface Constants {
    String ACTIVITY_FRAGMENT_REPLACE = "activity_fragment_replace";
    String CACHE_DIR = Environment.getExternalStorageDirectory() + "/Warenqi_RxCache";
    int SECTION_HEADER = 1092;
    int SECTION_CONTENT = 0;
    int LIST_PAGE_SIZE = 10;
    int IMAGE_PICK = 0x00111;
}
