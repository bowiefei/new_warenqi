package com.juying.warenqi.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.UiUtils;
import com.juying.warenqi.R;
import com.juying.warenqi.di.component.DaggerTaskStateListComponent;
import com.juying.warenqi.di.module.TaskStateListModule;
import com.juying.warenqi.enums.TaskStatus;
import com.juying.warenqi.mvp.contract.TaskStateListContract;
import com.juying.warenqi.mvp.presenter.TaskStateListPresenter;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;
import com.lcodecore.tkrefreshlayout.footer.LoadingView;
import com.lcodecore.tkrefreshlayout.header.SinaRefreshView;

import butterknife.BindView;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class TaskStateListActivity extends BaseActivity<TaskStateListPresenter> implements TaskStateListContract.View {


    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.refresh_layout)
    TwinklingRefreshLayout refreshLayout;
    @BindView(R.id.et_search_task_id)
    EditText etSearchTaskId;
    @BindView(R.id.btn_search)
    ImageView btnSearch;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    private int mPageNo = 1;
    private String mTaskStatus;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerTaskStateListComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .taskStateListModule(new TaskStateListModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_task_state_list; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        // 默认软键盘不弹出
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Intent intent = getIntent();
        String taskType = intent.getStringExtra("taskType");
        mTaskStatus = intent.getStringExtra("taskStatus");
        toolbarTitle.setText(mTaskStatus);
        if ("追评&问大家".equals(taskType))
            if ("待操作".equals(mTaskStatus))
                mTaskStatus = "待买手提交任务";
            else if ("商家处理中".equals(mTaskStatus))
                mTaskStatus = "待商家处理";
        switch (taskType) {
            case "人气游戏":
                mPresenter.getFlowTaskList(TaskStatus.getTaskStatus(mTaskStatus), mPageNo, null, true);
                break;
            case "优质游戏":
                mPresenter.getPayTaskList(TaskStatus.getTaskStatus(mTaskStatus), mPageNo, null, true);
                break;
            case "追评&问大家":
                mPresenter.getAskTaskList(TaskStatus.getTaskStatus(mTaskStatus), mPageNo, null, true);
                break;
        }

        btnSearch.setOnClickListener(view -> refreshLayout.startRefresh());

        refreshLayout.setHeaderView(new SinaRefreshView(this));
        refreshLayout.setBottomView(new LoadingView(this));
        refreshLayout.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onRefresh(TwinklingRefreshLayout refreshLayout) {
                KeyboardUtils.hideSoftInput(etSearchTaskId);
                String idStr = etSearchTaskId.getText().toString();
                Long id = null;
                try {
                    id = Long.parseLong(TextUtils.isEmpty(idStr) ? "0" : idStr);
                } catch (NumberFormatException e) {
                    showMessage("无效任务编号");
                    e.printStackTrace();
                    etSearchTaskId.setText(null);
                }
                if (id != null && id == 0) {
                    id = null;
                    etSearchTaskId.setText(null);
                }
                mPageNo = 1;
                switch (taskType) {
                    case "人气游戏":
                        mPresenter.getFlowTaskList(TaskStatus.getTaskStatus(mTaskStatus), mPageNo, id, true);
                        break;
                    case "优质游戏":
                        mPresenter.getPayTaskList(TaskStatus.getTaskStatus(mTaskStatus), mPageNo, id, true);
                        break;
                    case "追评&问大家":
                        mPresenter.getAskTaskList(TaskStatus.getTaskStatus(mTaskStatus), mPageNo, id, true);
                        break;
                }
            }

            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
                String idStr = etSearchTaskId.getText().toString();
                Long id = null;
                try {
                    id = Long.parseLong(TextUtils.isEmpty(idStr) ? "0" : idStr);
                } catch (NumberFormatException e) {
                    showMessage("无效任务编号");
                    e.printStackTrace();
                    etSearchTaskId.setText(null);
                }
                if (id != null && id == 0) {
                    id = null;
                    etSearchTaskId.setText(null);
                }
                switch (taskType) {
                    case "人气游戏":
                        mPresenter.getFlowTaskList(TaskStatus.getTaskStatus(mTaskStatus), ++mPageNo, id, false);
                        break;
                    case "优质游戏":
                        mPresenter.getPayTaskList(TaskStatus.getTaskStatus(mTaskStatus), ++mPageNo, id, false);
                        break;
                    case "追评&问大家":
                        mPresenter.getAskTaskList(TaskStatus.getTaskStatus(mTaskStatus), ++mPageNo, id, false);
                        break;
                }
            }
        });
        refreshLayout.startRefresh();
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {
        refreshLayout.finishRefreshing();
        refreshLayout.finishLoadmore();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        UiUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        UiUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }


    @Override
    public void setAdapter(BaseQuickAdapter adapter) {
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter.setEmptyView(R.layout.empty_view, (ViewGroup) rv.getParent());
    }

    @Override
    public void setNoMoreData(BaseQuickAdapter adapter) {
        TextView footerView = new TextView(this);
        footerView.setText("没有更多数据");
        footerView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                SizeUtils.dp2px(40)));
        footerView.setGravity(Gravity.CENTER);
        footerView.setTextSize(14);
        footerView.setTextColor(getResources().getColor(R.color.grey_333));
        adapter.setFooterView(footerView);
        adapter.notifyDataSetChanged();
    }

}