package com.juying.warenqi.mvp.model.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/5 16:23
 * </pre>
 */
public class AccountAuditMultiItem implements MultiItemEntity {
    public static final int EDITTEXT = 0;
    public static final int TEXT = 1;
    public static final int IMG = 2;
    private int itemType;
    private String title;
    private String content;
    private int status;
    private String imgUrl1;
    private String imgUrl2;

    public AccountAuditMultiItem(int itemType, String title) {
        this.itemType = itemType;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImgUrl1() {
        return imgUrl1;
    }

    public void setImgUrl1(String imgUrl1) {
        this.imgUrl1 = imgUrl1;
    }

    public String getImgUrl2() {
        return imgUrl2;
    }

    public void setImgUrl2(String imgUrl2) {
        this.imgUrl2 = imgUrl2;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    @Override
    public String toString() {
        return "AccountAuditMultiItem{" +
                "itemType=" + itemType +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", imgUrl1='" + imgUrl1 + '\'' +
                ", imgUrl2='" + imgUrl2 + '\'' +
                '}';
    }
}
