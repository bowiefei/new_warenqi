package com.juying.warenqi.mvp.contract;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jess.arms.mvp.IView;
import com.jess.arms.mvp.IModel;
import com.juying.warenqi.mvp.model.entity.AskTaskState;
import com.juying.warenqi.mvp.model.entity.FlowTaskState;
import com.juying.warenqi.mvp.model.entity.PayTaskState;

import java.util.List;

import io.reactivex.Observable;


public interface TaskStateListContract {
    //对于经常使用的关于UI的方法可以定义到BaseView中,如显示隐藏进度条,和显示文字消息
    interface View extends IView {
        void setAdapter(BaseQuickAdapter adapter);
        void setNoMoreData(BaseQuickAdapter adapter);
    }

    //Model层定义接口,外部只需关心model返回的数据,无需关心内部细节,及是否使用缓存
    interface Model extends IModel {
        Observable<List<FlowTaskState>> getFlowTaskStateList(String state, int pageNo,
                                                             Long id, long idLastQueried,
                                                             boolean isUpdate);

        Observable<List<PayTaskState>> getPayTaskStateList(String state, int pageNo,
                                                           Long id, long idLastQueried,
                                                           boolean isUpdate);

        Observable<List<AskTaskState>> getAskTaskStateList(String state, int pageNo,
                                                           Long id, long idLastQueried,
                                                           boolean isUpdate);
    }
}