package com.juying.warenqi.utils;

import java.util.Random;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/6 14:01
 * </pre>
 */
public class PicNameUtils {

    private PicNameUtils() {
    }

    //随机生成上传七牛需要的 key
    public static String getPicName() {
        String date = String.valueOf(System.currentTimeMillis());
        String str = getRandomString(8);
        return date + "_" + str + ".png";
    }

    //随机生成字符串
    private static String getRandomString(int length) { //length表示生成字符串的长度
        String base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

}
