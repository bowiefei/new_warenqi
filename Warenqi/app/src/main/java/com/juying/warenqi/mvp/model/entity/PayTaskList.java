package com.juying.warenqi.mvp.model.entity;

import java.util.List;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/1 15:54
 * </pre>
 */
public class PayTaskList {
    private boolean chat;
    private boolean coupon;
    private long createTime;
    private int days;
    private int disCount;
    private int expressIngot;
    private String expressType;
    private int extraReward;
    private boolean huabei;
    private int id;
    private String idStr;
    private int ingot;
    private int itemBuyNum;
    private long itemId;
    private String itemPicUrl;
    private int itemPrice;
    private String itemSubwayPicUrl;
    private String itemTitle;
    private String itemUrl;
    private List<?> items;
    private String itemPriceStr;
    private int maxTime;
    private String message;
    private int minTime;
    private int pageNo;
    private int pageSize;
    private int pledge;
    private int publishCount;
    private long publishTime;
    private String sdType;
    private int sellerId;
    private String sellerNick;
    private int shopId;
    private String shopName;
    private String sku;
    private int startIndex;
    private String status;
    private int sysRefound;
    private boolean taojinbi;
    private long taskId;
    private String taskIdStr;
    private String taskRequest;
    private int totalCount;
    private int totalPageCount;
    private long updateTime;

    public boolean isChat() {
        return chat;
    }

    public void setChat(boolean chat) {
        this.chat = chat;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getDisCount() {
        return disCount;
    }

    public void setDisCount(int disCount) {
        this.disCount = disCount;
    }

    public int getExpressIngot() {
        return expressIngot;
    }

    public void setExpressIngot(int expressIngot) {
        this.expressIngot = expressIngot;
    }

    public String getExpressType() {
        return expressType;
    }

    public void setExpressType(String expressType) {
        this.expressType = expressType;
    }

    public int getExtraReward() {
        return extraReward;
    }

    public void setExtraReward(int extraReward) {
        this.extraReward = extraReward;
    }

    public boolean isHuabei() {
        return huabei;
    }

    public void setHuabei(boolean huabei) {
        this.huabei = huabei;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdStr() {
        return idStr;
    }

    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    public int getIngot() {
        return ingot;
    }

    public void setIngot(int ingot) {
        this.ingot = ingot;
    }

    public int getItemBuyNum() {
        return itemBuyNum;
    }

    public void setItemBuyNum(int itemBuyNum) {
        this.itemBuyNum = itemBuyNum;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemSubwayPicUrl() {
        return itemSubwayPicUrl;
    }

    public void setItemSubwayPicUrl(String itemSubwayPicUrl) {
        this.itemSubwayPicUrl = itemSubwayPicUrl;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public List<?> getItems() {
        return items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }

    public String getItemPriceStr() {
        return itemPriceStr;
    }

    public void setItemPriceStr(String itemPriceStr) {
        this.itemPriceStr = itemPriceStr;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPledge() {
        return pledge;
    }

    public void setPledge(int pledge) {
        this.pledge = pledge;
    }

    public int getPublishCount() {
        return publishCount;
    }

    public void setPublishCount(int publishCount) {
        this.publishCount = publishCount;
    }

    public long getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(long publishTime) {
        this.publishTime = publishTime;
    }

    public String getSdType() {
        return sdType;
    }

    public void setSdType(String sdType) {
        this.sdType = sdType;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerNick() {
        return sellerNick;
    }

    public void setSellerNick(String sellerNick) {
        this.sellerNick = sellerNick;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSysRefound() {
        return sysRefound;
    }

    public void setSysRefound(int sysRefound) {
        this.sysRefound = sysRefound;
    }

    public boolean isTaojinbi() {
        return taojinbi;
    }

    public void setTaojinbi(boolean taojinbi) {
        this.taojinbi = taojinbi;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getTaskIdStr() {
        return taskIdStr;
    }

    public void setTaskIdStr(String taskIdStr) {
        this.taskIdStr = taskIdStr;
    }

    public String getTaskRequest() {
        return taskRequest;
    }

    public void setTaskRequest(String taskRequest) {
        this.taskRequest = taskRequest;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
}
