package com.juying.warenqi.enums;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/2 16:27
 * </pre>
 */
public enum TaskTypeEnum {
    ORDER("常规任务"),
    SUBWAY("直通车任务"),
    TKL("淘口令任务"),
    JHS("聚划算任务"),
    TQG("淘抢购任务"),
    TKMP("淘客秒拍任务"),
    TJB("淘金币任务"),
    TTTJ("天天特价任务"),
    RWM("二维码任务"),
    ZWTG("活动推广任务"),
    DIANFU("垫付任务"),
    DAYSDIANFU("多天任务");

    TaskTypeEnum(String typeName) {
        this.name = typeName;
    }

    private final String name;

    public String getName() {
        return name;
    }

    public static String getTaskType(String name) {
        for (TaskTypeEnum taskType : TaskTypeEnum.values()) {
            if (name.equals(taskType.getName())) {
                return taskType.name();
            }
        }
        return null;
    }
}
