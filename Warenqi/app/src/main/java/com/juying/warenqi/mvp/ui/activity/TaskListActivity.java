package com.juying.warenqi.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.blankj.utilcode.util.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.UiUtils;
import com.juying.warenqi.R;
import com.juying.warenqi.app.AccountTabEntity;
import com.juying.warenqi.di.component.DaggerTaskListComponent;
import com.juying.warenqi.di.module.TaskListModule;
import com.juying.warenqi.enums.TaskTypes;
import com.juying.warenqi.mvp.contract.TaskListContract;
import com.juying.warenqi.mvp.model.entity.TaobaoAccount;
import com.juying.warenqi.mvp.presenter.TaskListPresenter;
import com.lcodecore.tkrefreshlayout.RefreshListenerAdapter;
import com.lcodecore.tkrefreshlayout.TwinklingRefreshLayout;
import com.lcodecore.tkrefreshlayout.footer.LoadingView;
import com.lcodecore.tkrefreshlayout.header.SinaRefreshView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.jess.arms.utils.Preconditions.checkNotNull;


public class TaskListActivity extends BaseActivity<TaskListPresenter> implements TaskListContract.View {


    @BindView(R.id.tab_task_list)
    CommonTabLayout tabTaskList;
    @BindView(R.id.btn_add_buyer_account)
    Button btnAddBuyerAccount;
    @BindView(R.id.rv_task_list)
    RecyclerView rvTaskList;
    @BindView(R.id.refresh_layout)
    TwinklingRefreshLayout refreshLayout;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private String mTaskType;
    private int pageNo = 1;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerTaskListComponent //如找不到该类,请编译一下项目
                .builder()
                .appComponent(appComponent)
                .taskListModule(new TaskListModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_task_list; //如果你不需要框架帮你设置 setContentView(id) 需要自行设置,请返回 0
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mPresenter.getAccountList();
        Intent intent = getIntent();
        String taskType = intent.getStringExtra("taskType");
        toolbarTitle.setText(taskType);
        switch (taskType) {
            case "常规游戏":
                mTaskType = TaskTypes.NORMAL;
                break;
            case "直通车游戏":
                mTaskType = TaskTypes.DIRECT;
                break;
            case "活动游戏":
                mTaskType = TaskTypes.ACTIVITY;
                break;
            case "优质游戏":
                mTaskType = TaskTypes.ADVANCE_PAY;
                break;
            case "多天游戏":
                mTaskType = TaskTypes.SEVERAL_DAYS;
                break;
        }
        refreshLayout.setHeaderView(new SinaRefreshView(this));
        refreshLayout.setBottomView(new LoadingView(this));
        refreshLayout.setOnRefreshListener(new RefreshListenerAdapter() {
            @Override
            public void onRefresh(TwinklingRefreshLayout refreshLayout) {
                int currentTab = tabTaskList.getCurrentTab();
                long accountId = ((AccountTabEntity) mTabEntities.get(currentTab)).getAccountId();
                pageNo = 1;
                switch (mTaskType) {
                    case TaskTypes.NORMAL:
                    case TaskTypes.DIRECT:
                    case TaskTypes.ACTIVITY:
                        mPresenter.getFlowTaskList(1, mTaskType, accountId, true);
                        break;
                    case TaskTypes.ADVANCE_PAY:
                    case TaskTypes.SEVERAL_DAYS:
                        mPresenter.getPayTaskList(1, mTaskType, accountId, true);
                        break;
                }
            }

            @Override
            public void onLoadMore(TwinklingRefreshLayout refreshLayout) {
                int currentTab = tabTaskList.getCurrentTab();
                long accountId = ((AccountTabEntity) mTabEntities.get(currentTab)).getAccountId();
                switch (mTaskType) {
                    case TaskTypes.NORMAL:
                    case TaskTypes.DIRECT:
                    case TaskTypes.ACTIVITY:
                        mPresenter.getFlowTaskList(++pageNo, mTaskType, accountId, false);
                        break;
                    case TaskTypes.ADVANCE_PAY:
                    case TaskTypes.SEVERAL_DAYS:
                        mPresenter.getPayTaskList(++pageNo, mTaskType, accountId, false);
                        break;
                }
            }
        });
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {
        refreshLayout.finishRefreshing();
        refreshLayout.finishLoadmore();
    }

    @Override
    public void showMessage(@NonNull String message) {
        checkNotNull(message);
        UiUtils.snackbarText(message);
    }

    @Override
    public void launchActivity(@NonNull Intent intent) {
        checkNotNull(intent);
        UiUtils.startActivity(intent);
    }

    @Override
    public void killMyself() {
        finish();
    }

    @OnClick(R.id.btn_add_buyer_account)
    public void onViewClicked() {

    }

    @Override
    public void showError() {
        refreshLayout.finishRefreshing();
        refreshLayout.finishLoadmore();
    }

    @Override
    public void setAccountTab(List<TaobaoAccount> accountList) {
        mTabEntities.clear();
        for (TaobaoAccount taobaoAccount : accountList) {
            mTabEntities.add(new AccountTabEntity(taobaoAccount.getNick(),
                    taobaoAccount.getId(), taobaoAccount.getStatus().getTitle()));
        }
        tabTaskList.setTabData(mTabEntities);
        tabTaskList.setOnTabSelectListener(mOnTabSelectListener);
        tabTaskList.setCurrentTab(0);
        refreshLayout.startRefresh();
    }

    @Override
    public void setAdapter(BaseQuickAdapter adapter) {
        rvTaskList.setAdapter(adapter);
        rvTaskList.setLayoutManager(new LinearLayoutManager(this));
        adapter.setEmptyView(R.layout.empty_view, (ViewGroup) rvTaskList.getParent());
    }

    @Override
    public void setNoMoreData(BaseQuickAdapter adapter) {
        TextView footerView = new TextView(this);
        footerView.setText("没有更多数据");
        footerView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                SizeUtils.dp2px(40)));
        footerView.setGravity(Gravity.CENTER);
        footerView.setTextSize(14);
        footerView.setTextColor(getResources().getColor(R.color.grey_333));
        adapter.setFooterView(footerView);
        adapter.notifyDataSetChanged();
    }

    private OnTabSelectListener mOnTabSelectListener = new OnTabSelectListener() {
        @Override
        public void onTabSelect(int position) {
            refreshLayout.startRefresh();
        }

        @Override
        public void onTabReselect(int position) {

        }
    };
}