package com.juying.warenqi.mvp.ui.adapter;

import android.support.annotation.Nullable;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.ui.holder.BaseAutoLayoutHolder;
import com.juying.warenqi.view.popupwindows.PopTitle;

import java.util.List;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/6/29 11:48
 * </pre>
 */
public class PopListAdapter extends BaseQuickAdapter<PopTitle, BaseAutoLayoutHolder> {
    public PopListAdapter(@Nullable List<PopTitle> data) {
        super(R.layout.item_pop_list, data);
    }

    @Override
    protected void convert(BaseAutoLayoutHolder helper, PopTitle item) {
        helper.setText(R.id.tv_pop_item, item.getTitle())
                .setTextColor(R.id.tv_pop_item, mContext.getApplicationContext()
                        .getResources().getColor(item.getTextColor()));
        ((TextView) helper.getView(R.id.tv_pop_item))
                .setTextSize(item.getTextSize());
    }
}
