package com.juying.warenqi.mvp.presenter;

import android.app.Application;
import android.content.Intent;
import android.text.TextUtils;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.widget.imageloader.ImageLoader;
import com.juying.warenqi.R;
import com.juying.warenqi.enums.AccountStatus;
import com.juying.warenqi.mvp.contract.ListContract;
import com.juying.warenqi.mvp.model.api.Api;
import com.juying.warenqi.mvp.model.entity.AccountAuditInfo;
import com.juying.warenqi.mvp.model.entity.AccountAuditMultiItem;
import com.juying.warenqi.mvp.model.entity.AccountInfoSection;
import com.juying.warenqi.mvp.model.entity.AccountInfoSectionContent;
import com.juying.warenqi.mvp.model.entity.BaseBean;
import com.juying.warenqi.mvp.model.entity.TaobaoAccount;
import com.juying.warenqi.mvp.ui.activity.LoginActivity;
import com.juying.warenqi.mvp.ui.adapter.CommonAdapter;
import com.juying.warenqi.mvp.ui.adapter.MultiItemAdapter;
import com.juying.warenqi.mvp.ui.adapter.PersonalProfileAdapter;
import com.juying.warenqi.utils.AppCacheUtils;
import com.juying.warenqi.utils.PicNameUtils;
import com.juying.warenqi.utils.RxUtils;
import com.qiniu.android.storage.UploadManager;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class ListPresenter extends BasePresenter<ListContract.Model, ListContract.View> {
    private RxErrorHandler mErrorHandler;
    private Application mApplication;
    private ImageLoader mImageLoader;
    private AppManager mAppManager;
    private BaseQuickAdapter mAdapter;
    private List<AccountInfoSectionContent> mList = new ArrayList<>();
    private List<AccountInfoSection> mSectionList = new ArrayList<>();

    @Inject
    public ListPresenter(ListContract.Model model, ListContract.View rootView
            , RxErrorHandler handler, Application application
            , ImageLoader imageLoader, AppManager appManager) {
        super(model, rootView);
        this.mErrorHandler = handler;
        this.mApplication = application;
        this.mImageLoader = imageLoader;
        this.mAppManager = appManager;
    }

    public void getSettingInfo() {
        mList.clear();

        if (mAdapter == null) {
            mAdapter = new CommonAdapter<>(R.layout.item_account_info_content, mList);
        }

        mAdapter.setNewData(mList);
        mRootView.setAdapter(mAdapter);
        mRootView.addFooterButton(mAdapter, "退出登录");

        mList.add(new AccountInfoSectionContent("关于我们"));
        AccountInfoSectionContent versionContent = new AccountInfoSectionContent("版本信息");
        versionContent.setExtras(AppUtils.getAppVersionName());
        mList.add(versionContent);
        String totalCacheSize = null;
        try {
            totalCacheSize = AppCacheUtils.getTotalCacheSize(mApplication);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(totalCacheSize)) totalCacheSize = "无";
        mList.add(new AccountInfoSectionContent("清除缓存" + "（" + totalCacheSize + "）"));
        Observable.just(mAdapter)
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<BaseQuickAdapter>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseQuickAdapter baseQuickAdapter) {
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
    }

    /**
     * 退出登录
     */
    public void logout() {
        mModel.logout()
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<BaseBean>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseBean baseBean) {
                        if (baseBean.isSuccess()) {
                            SPUtils.getInstance().clear();
                            mAppManager.killAll();
                            mRootView.launchActivity(new Intent(mApplication, LoginActivity.class));
                        }
                    }
                });
    }

    /**
     * 获取个人资料
     */
    public void getPersonalProfile() {
        mSectionList.clear();

        if (mAdapter == null) {
            mAdapter = new PersonalProfileAdapter(mSectionList);
        }

        mAdapter.setNewData(mSectionList);
        mRootView.setAdapter(mAdapter);

        mModel.getPersonalProfile()
                .compose(RxUtils.applySchedulers(mRootView))
                .flatMap(accountInfo -> {
                    AccountInfoSectionContent avatar = new AccountInfoSectionContent("头像");
                    avatar.setExtras(accountInfo.getHeadPic());
                    AccountInfoSection avatarSection = new AccountInfoSection(avatar);

                    AccountInfoSectionContent district = new AccountInfoSectionContent("地区");
                    district.setExtras(accountInfo.getState() + "，" + accountInfo.getCity() + "，"
                            + accountInfo.getRegion());
                    AccountInfoSection districtSection = new AccountInfoSection(district);

                    AccountInfoSectionContent qq = new AccountInfoSectionContent("QQ号码");
                    qq.setExtras(accountInfo.getQq());
                    AccountInfoSection qqSection = new AccountInfoSection(qq);

                    AccountInfoSectionContent phone = new AccountInfoSectionContent("手机号码");
                    phone.setExtras(accountInfo.getMobile());
                    AccountInfoSection phoneSection = new AccountInfoSection(phone);

                    AccountInfoSection line = new AccountInfoSection(true, null);

                    AccountInfoSectionContent loginPwd = new AccountInfoSectionContent("登录密码");
                    AccountInfoSection loginSection = new AccountInfoSection(loginPwd);

                    AccountInfoSectionContent payPwd = new AccountInfoSectionContent("支付密码");
                    AccountInfoSection paySection = new AccountInfoSection(payPwd);

                    mSectionList.add(avatarSection);
                    mSectionList.add(districtSection);
                    mSectionList.add(qqSection);
                    mSectionList.add(phoneSection);
                    mSectionList.add(line);
                    mSectionList.add(loginSection);
                    mSectionList.add(paySection);

                    return Observable.just(mAdapter);
                })
                .subscribe(new ErrorHandleSubscriber<BaseQuickAdapter>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseQuickAdapter baseQuickAdapter) {
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
    }

    public void getAccountList(String type) {
        mList.clear();

        if (mAdapter == null) {
            mAdapter = new CommonAdapter<>(R.layout.item_account_info_content, mList);
        }

        mAdapter.setNewData(mList);
        mRootView.setAdapter(mAdapter);

        mModel.getAccountList()
                .compose(RxUtils.applySchedulers(mRootView))
                .flatMap(taobaoAccounts -> {
                    for (TaobaoAccount taobaoAccount : taobaoAccounts) {
                        AccountInfoSectionContent account = new AccountInfoSectionContent(taobaoAccount.getNick());
                        if (type.equals("淘宝账号")) {
                            account.setLabelResId(0);
                            account.setExtras(taobaoAccount.getStatus().getTitle());
                        } else {
                            account.setLabelResId(1);
                            AccountStatus huabeiExamine = taobaoAccount.getHuabeiExamine();
                            account.setExtras(huabeiExamine == null ? "null" : huabeiExamine.getTitle());
                        }
                        mList.add(account);
                    }
                    return Observable.just(mAdapter);
                })
                .subscribe(new ErrorHandleSubscriber<BaseQuickAdapter>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseQuickAdapter baseQuickAdapter) {
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
    }

    public void getAccountAuditInfo() {
        List<AccountAuditMultiItem> list = new ArrayList<>();

        if (mAdapter == null) {
            mAdapter = new MultiItemAdapter(list);
        }

        AccountAuditMultiItem name = new AccountAuditMultiItem(AccountAuditMultiItem.EDITTEXT, "真实姓名");
        AccountAuditMultiItem auditType = new AccountAuditMultiItem(AccountAuditMultiItem.TEXT, "证件类型");
        auditType.setContent("身份证");
        AccountAuditMultiItem idCardNum = new AccountAuditMultiItem(AccountAuditMultiItem.EDITTEXT, "证件号码");
        AccountAuditMultiItem idCardImg = new AccountAuditMultiItem(AccountAuditMultiItem.IMG, "证件照片");
        list.add(name);
        list.add(auditType);
        list.add(idCardNum);
        list.add(idCardImg);
        mAdapter.setNewData(list);
        mRootView.setAdapter(mAdapter);

        mModel.getAccountAuditInfo()
                .compose(RxUtils.applySchedulers(mRootView))
                .flatMap(auditInfoBaseBean -> {
                    if (auditInfoBaseBean.isSuccess()) {
                        AccountAuditInfo accountAuditInfo = auditInfoBaseBean.getResults();
                        name.setContent(accountAuditInfo.getName());
                        idCardNum.setContent(accountAuditInfo.getIdcard());
                        idCardImg.setImgUrl1(accountAuditInfo.getHandImg());
                        idCardImg.setImgUrl2(accountAuditInfo.getIdcardImg());

                        name.setStatus(accountAuditInfo.getStatus());
                        idCardNum.setStatus(accountAuditInfo.getStatus());
                        idCardImg.setStatus(accountAuditInfo.getStatus());
                        switch (accountAuditInfo.getStatus()) {
                            case 0:
                                name.setContent("请填写您的真实姓名");
                                idCardNum.setContent("请输入您的身份证号码");
                                mRootView.addFooterButton(mAdapter, "提交审核");
                                break;
                            case 1:
                                idCardImg.setContent("审核已通过");
                                mAdapter.removeAllFooterView();
                                break;
                            case 2:
                                String memo = accountAuditInfo.getMemo();
                                Gson gson = new Gson();
                                String replace = memo.replace("\"memo\":\"[", "\"memo\":[").replace("]\"", "]").replace("\\", "");
                                AccountAuditInfo.MemoBean memoBean = gson.fromJson(replace, AccountAuditInfo.MemoBean.class);
                                String builder = "审核不通过原因：" +
                                        memoBean.getContent() +
                                        "\n审核不通过时间：" +
                                        TimeUtils.millis2String(memoBean.getDateTime());
                                idCardImg.setContent(builder);
                                mRootView.addFooterButton(mAdapter, "提交审核");
                                break;
                        }
                    } else {
                        name.setContent("请填写您的真实姓名");
                        idCardNum.setContent("请输入您的身份证号码");
                        mRootView.addFooterButton(mAdapter, "提交审核");
                    }
                    return Observable.just(mAdapter);
                })
                .subscribe(new ErrorHandleSubscriber<BaseQuickAdapter>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseQuickAdapter baseQuickAdapter) {
                        baseQuickAdapter.notifyDataSetChanged();
                    }
                });
    }


    public void uploadAuditImg(String photoPath, int viewId) {
        mRootView.showUploadDialog();
        UploadManager uploadManager = new UploadManager();
        uploadManager.put(photoPath, PicNameUtils.getPicName(),
                SPUtils.getInstance().getString("qiniu_token"), (key, info, response) -> {
                    if (info.isOK()) {
                        try {
                            String imgKey = response.getString("key");
                            List data = mAdapter.getData();
                            AccountAuditMultiItem item =
                                    (AccountAuditMultiItem) data.get(data.size() - 1);
                            if (viewId != 0 && viewId == R.id.rl_img1) {
                                item.setImgUrl1(Api.QINIU_URL + imgKey);
                            } else if (viewId != 0 && viewId == R.id.rl_img2) {
                                item.setImgUrl2(Api.QINIU_URL + imgKey);
                            }
                            mAdapter.notifyItemChanged(data.size() - 1);
                            mRootView.dismissUploadDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, null);
    }

    public void submitAuditInfo() {
        String name = null;
        String idcard = null;
        String idcardImg = null;
        String handImg = null;
        List data = mAdapter.getData();
        for (AccountAuditMultiItem item : (List<AccountAuditMultiItem>) data) {
            switch (item.getTitle()) {
                case "真实姓名":
                    String content = item.getContent();
                    if (!RegexUtils.isZh(content)) {
                        mRootView.showMessage("请输入正确的姓名");
                        return;
                    } else {
                        name = content;
                    }
                    break;
                case "证件号码":
                    idcard = item.getContent();
                    break;
                case "证件照片":
                    String imgUrl1 = item.getImgUrl1();
                    String imgUrl2 = item.getImgUrl2();
                    if (TextUtils.isEmpty(imgUrl1)) {
                        mRootView.showMessage("请上传身份证正面照片");
                        return;
                    } else {
                        idcardImg = imgUrl1;
                    }
                    if (TextUtils.isEmpty(imgUrl2)) {
                        mRootView.showMessage("请上传手持身份证照片");
                        return;
                    } else {
                        handImg = imgUrl2;
                    }
                    break;
            }
        }
        mModel.submitAuditInfo(name, idcard, idcardImg, handImg)
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<BaseBean>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull BaseBean baseBean) {
                        if (baseBean.isSuccess()) {
                            ToastUtils.showShort("提交成功");
                            mRootView.killMyself();
                        } else {
                            mRootView.showMessage(baseBean.getMsg());
                        }
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
        this.mAdapter = null;
        this.mList = null;
        this.mSectionList = null;
    }

}