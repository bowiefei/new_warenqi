package com.juying.warenqi.di.module;

import com.jess.arms.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.juying.warenqi.mvp.contract.ListContract;
import com.juying.warenqi.mvp.model.ListModel;


@Module
public class ListModule {
    private ListContract.View view;

    /**
     * 构建ListModule时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     *
     * @param view
     */
    public ListModule(ListContract.View view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    ListContract.View provideListView() {
        return this.view;
    }

    @ActivityScope
    @Provides
    ListContract.Model provideListModel(ListModel model) {
        return model;
    }
}