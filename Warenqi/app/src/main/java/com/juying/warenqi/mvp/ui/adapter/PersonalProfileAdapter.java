package com.juying.warenqi.mvp.ui.adapter;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blankj.utilcode.util.SizeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.juying.warenqi.R;
import com.juying.warenqi.mvp.model.entity.AccountInfoSection;
import com.juying.warenqi.mvp.ui.holder.BaseAutoLayoutHolder;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * <pre>
 * Author: @Administrator
 * Description:
 * Date: 2017/7/5 12:49
 * </pre>
 */
public class PersonalProfileAdapter extends BaseSectionQuickAdapter<AccountInfoSection, BaseAutoLayoutHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public PersonalProfileAdapter(List<AccountInfoSection> data) {
        super(R.layout.item_account_info_content, R.layout.account_info_divide_header, data);
    }

    @Override
    protected void convertHead(BaseAutoLayoutHolder helper, AccountInfoSection item) {
        View view = helper.getView(R.id.account_info_header_view);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = 15;
        view.setLayoutParams(layoutParams);
    }

    @Override
    protected void convert(BaseAutoLayoutHolder helper, AccountInfoSection item) {
        helper.setText(R.id.tv_account_item_title, item.t.getTitle())
                .setText(R.id.tv_account_item_extra, item.t.getExtras())
                .setTextColor(R.id.tv_account_item_extra, mContext.getApplicationContext()
                        .getResources().getColor(R.color.grey_999));
        View view = helper.getView(R.id.item_container);
        view.setPadding(SizeUtils.dp2px(15), 0, 0, 0);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = SizeUtils.dp2px(50);
        view.setLayoutParams(layoutParams);
        ((TextView) helper.getView(R.id.tv_account_item_title)).setTextSize(16);
        TextView textView = helper.getView(R.id.tv_account_item_extra);
        textView.setTextSize(14);
        if (!TextUtils.isEmpty(item.t.getExtras()) && item.t.getExtras().startsWith("http")) {
            Glide.with(mContext.getApplicationContext()).load(item.t.getExtras())
                    .bitmapTransform(new CropCircleTransformation(mContext.getApplicationContext()))
                    .into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                            resource.setBounds(0, 0, SizeUtils.dp2px(46), SizeUtils.dp2px(46));
                            Drawable drawable = mContext.getApplicationContext().getResources()
                                    .getDrawable(R.drawable.ic_arrow_right_grey_24dp);
                            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                            textView.setCompoundDrawables(resource, null, drawable, null);
                            textView.setText(null);
                        }
                    });
        }
    }
}
