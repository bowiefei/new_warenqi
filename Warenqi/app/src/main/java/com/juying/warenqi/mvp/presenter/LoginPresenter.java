package com.juying.warenqi.mvp.presenter;

import android.app.Application;
import android.content.Intent;

import com.blankj.utilcode.util.SPUtils;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.AppManager;
import com.jess.arms.mvp.BasePresenter;
import com.jess.arms.widget.imageloader.ImageLoader;
import com.juying.warenqi.mvp.contract.LoginContract;
import com.juying.warenqi.mvp.model.entity.AccountInfo;
import com.juying.warenqi.mvp.ui.activity.MainActivity;
import com.juying.warenqi.utils.RxUtils;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;


@ActivityScope
public class LoginPresenter extends BasePresenter<LoginContract.Model, LoginContract.View> {
    private RxErrorHandler mErrorHandler;
    private Application mApplication;
    private ImageLoader mImageLoader;
    private AppManager mAppManager;

    @Inject
    public LoginPresenter(LoginContract.Model model, LoginContract.View rootView
            , RxErrorHandler handler, Application application
            , ImageLoader imageLoader, AppManager appManager) {
        super(model, rootView);
        this.mErrorHandler = handler;
        this.mApplication = application;
        this.mImageLoader = imageLoader;
        this.mAppManager = appManager;
    }

    public void getQiniuToken() {
        mModel.getQiniuToken()
                .compose(RxUtils.applySchedulers(mRootView))
                .subscribe(new ErrorHandleSubscriber<String>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull String s) {
                        SPUtils.getInstance().put("qiniu_token", s);
                    }
                });
    }

    public void login(String username, String password) {
        mModel.login(username, password)
                .compose(RxUtils.applySchedulers(mRootView))
                .compose(RxUtils.bindToLifecycle(mRootView))
                .subscribe(infoBaseBean -> {
                    if (infoBaseBean.isSuccess()) {
                        mRootView.showMessage("登录成功");
                        SPUtils.getInstance().put("username", username);
                        SPUtils.getInstance().put("password", password);
                        saveAccountInfo(infoBaseBean.getResults());
                        Intent intent = new Intent();
                        intent.setClass(mApplication, MainActivity.class);
                        mRootView.launchActivity(intent);
                    }
                });
    }

    private void saveAccountInfo(AccountInfo accountInfo) {
        SPUtils sp = SPUtils.getInstance();
        sp.put("isLogin", true);
        sp.put("userId", accountInfo.getId());
        sp.put("nick", accountInfo.getNick());
        sp.put("payPass", accountInfo.getPayPassword());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mErrorHandler = null;
        this.mAppManager = null;
        this.mImageLoader = null;
        this.mApplication = null;
    }

}