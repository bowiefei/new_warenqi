package com.juying.warenqi.mvp.model.entity;

import com.juying.warenqi.enums.TaskStatus;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/6/25 16:17
 * </pre>
 */
public class MyTaskStateContent {
    private TaskStatus taskStatus;
    private String taskType;
    private int taskTaskCount;

    public MyTaskStateContent(String taskType, TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
        this.taskType = taskType;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public int getTaskTaskCount() {
        return taskTaskCount;
    }

    public void setTaskTaskCount(int taskTaskCount) {
        this.taskTaskCount = taskTaskCount;
    }

}
