package com.juying.warenqi.mvp.model.entity;

import com.juying.warenqi.enums.TaskStatus;

import java.util.List;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/2 22:16
 * </pre>
 */
public class PayTaskState {
    private int afterCommentProfit;
    private int buyerAccountId;
    private String buyerAccountNick;
    private int buyerId;
    private String buyerNick;
    private boolean chat;
    private int commentProfit;
    private int comssionProfit;
    private boolean coupon;
    private long createTime;
    private boolean delay;
    private int expressIngot;
    private int expressProfit;
    private String expressType;
    private int flowId;
    private boolean huabei;
    private int id;
    private int ingot;
    private int itemBuyNum;
    private long itemId;
    private String itemPicUrl;
    private int itemPrice;
    private String itemSubwayPicUrl;
    private String itemTitle;
    private String itemUrl;
    private String message;
    private String orderId;
    private String orderImg;
    private String otherImg;
    private int pageNo;
    private int pageSize;
    private int paidFee;
    private long payTime;
    private int postage;
    private int refoundProfit;
    private int sdTaskId;
    private String sdType;
    private int sellerId;
    private String sellerNick;
    private int shopId;
    private String shopName;
    private String sku;
    private int startIndex;
    private TaskStatus status;
    private long takeTime;
    private boolean taojinbi;
    private long taskId;
    private String taskRequest;
    private int totalCount;
    private int totalPageCount;
    private List<?> items;
    private long firstTime;
    private long secondTime;
    private long thirdTime;
    private int extraReward;
    private String itemPriceStr;

    public int getAfterCommentProfit() {
        return afterCommentProfit;
    }

    public void setAfterCommentProfit(int afterCommentProfit) {
        this.afterCommentProfit = afterCommentProfit;
    }

    public int getBuyerAccountId() {
        return buyerAccountId;
    }

    public void setBuyerAccountId(int buyerAccountId) {
        this.buyerAccountId = buyerAccountId;
    }

    public String getBuyerAccountNick() {
        return buyerAccountNick;
    }

    public void setBuyerAccountNick(String buyerAccountNick) {
        this.buyerAccountNick = buyerAccountNick;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerNick() {
        return buyerNick;
    }

    public void setBuyerNick(String buyerNick) {
        this.buyerNick = buyerNick;
    }

    public boolean isChat() {
        return chat;
    }

    public void setChat(boolean chat) {
        this.chat = chat;
    }

    public int getCommentProfit() {
        return commentProfit;
    }

    public void setCommentProfit(int commentProfit) {
        this.commentProfit = commentProfit;
    }

    public int getComssionProfit() {
        return comssionProfit;
    }

    public void setComssionProfit(int comssionProfit) {
        this.comssionProfit = comssionProfit;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public boolean isDelay() {
        return delay;
    }

    public void setDelay(boolean delay) {
        this.delay = delay;
    }

    public int getExpressIngot() {
        return expressIngot;
    }

    public void setExpressIngot(int expressIngot) {
        this.expressIngot = expressIngot;
    }

    public int getExpressProfit() {
        return expressProfit;
    }

    public void setExpressProfit(int expressProfit) {
        this.expressProfit = expressProfit;
    }

    public String getExpressType() {
        return expressType;
    }

    public void setExpressType(String expressType) {
        this.expressType = expressType;
    }

    public int getFlowId() {
        return flowId;
    }

    public void setFlowId(int flowId) {
        this.flowId = flowId;
    }

    public boolean isHuabei() {
        return huabei;
    }

    public void setHuabei(boolean huabei) {
        this.huabei = huabei;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIngot() {
        return ingot;
    }

    public void setIngot(int ingot) {
        this.ingot = ingot;
    }

    public int getItemBuyNum() {
        return itemBuyNum;
    }

    public void setItemBuyNum(int itemBuyNum) {
        this.itemBuyNum = itemBuyNum;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemSubwayPicUrl() {
        return itemSubwayPicUrl;
    }

    public void setItemSubwayPicUrl(String itemSubwayPicUrl) {
        this.itemSubwayPicUrl = itemSubwayPicUrl;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderImg() {
        return orderImg;
    }

    public void setOrderImg(String orderImg) {
        this.orderImg = orderImg;
    }

    public String getOtherImg() {
        return otherImg;
    }

    public void setOtherImg(String otherImg) {
        this.otherImg = otherImg;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPaidFee() {
        return paidFee;
    }

    public void setPaidFee(int paidFee) {
        this.paidFee = paidFee;
    }

    public long getPayTime() {
        return payTime;
    }

    public void setPayTime(long payTime) {
        this.payTime = payTime;
    }

    public int getPostage() {
        return postage;
    }

    public void setPostage(int postage) {
        this.postage = postage;
    }

    public int getRefoundProfit() {
        return refoundProfit;
    }

    public void setRefoundProfit(int refoundProfit) {
        this.refoundProfit = refoundProfit;
    }

    public int getSdTaskId() {
        return sdTaskId;
    }

    public void setSdTaskId(int sdTaskId) {
        this.sdTaskId = sdTaskId;
    }

    public String getSdType() {
        return sdType;
    }

    public void setSdType(String sdType) {
        this.sdType = sdType;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerNick() {
        return sellerNick;
    }

    public void setSellerNick(String sellerNick) {
        this.sellerNick = sellerNick;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public long getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(long takeTime) {
        this.takeTime = takeTime;
    }

    public boolean isTaojinbi() {
        return taojinbi;
    }

    public void setTaojinbi(boolean taojinbi) {
        this.taojinbi = taojinbi;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getTaskRequest() {
        return taskRequest;
    }

    public void setTaskRequest(String taskRequest) {
        this.taskRequest = taskRequest;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public List<?> getItems() {
        return items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }

    public long getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(long firstTime) {
        this.firstTime = firstTime;
    }

    public long getSecondTime() {
        return secondTime;
    }

    public void setSecondTime(long secondTime) {
        this.secondTime = secondTime;
    }

    public long getThirdTime() {
        return thirdTime;
    }

    public void setThirdTime(long thirdTime) {
        this.thirdTime = thirdTime;
    }

    public int getExtraReward() {
        return extraReward;
    }

    public void setExtraReward(int extraReward) {
        this.extraReward = extraReward;
    }

    public String getItemPriceStr() {
        return itemPriceStr;
    }

    public void setItemPriceStr(String itemPriceStr) {
        this.itemPriceStr = itemPriceStr;
    }
}
