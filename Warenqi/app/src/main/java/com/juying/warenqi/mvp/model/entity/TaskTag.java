package com.juying.warenqi.mvp.model.entity;

import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;

/**
 * <pre>
 * Author: @LvYan
 * Description:
 * Date: 2017/7/1 14:50
 * </pre>
 */
public class TaskTag {
    private String title;
    private int textSize;
    private int padding;
    @ColorInt
    private int textColor;
    @DrawableRes
    private int backgroundRes;

    public TaskTag(String title, int textColor, int backgroundRes) {
        this.title = title;
        this.textColor = textColor;
        this.backgroundRes = backgroundRes;
    }

    public TaskTag(String title, int textSize, int padding, int textColor, int backgroundRes) {
        this.title = title;
        this.textSize = textSize;
        this.padding = padding;
        this.textColor = textColor;
        this.backgroundRes = backgroundRes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public int getPadding() {
        return padding;
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(@ColorInt int textColor) {
        this.textColor = textColor;
    }

    public int getBackgroundRes() {
        return backgroundRes;
    }

    public void setBackgroundRes(@DrawableRes int backgroundRes) {
        this.backgroundRes = backgroundRes;
    }
}
